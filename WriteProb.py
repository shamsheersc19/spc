# =============================================================================
# External Python modules
# =============================================================================
import os
import textwrap
import numpy as np
import math
# =============================================================================
# Output file path
# =============================================================================
filepath = os.getcwd()
# =============================================================================
# Main function
# =============================================================================
def WriteProb(file_name, n_vars, d_poly, structure=None, tol=5e-7):
    """
    Function for generating the problem and outputting it to a file.

    Parameters
    ----------
    file_name : str
        Output file name.

    n_vars : numpy array
        An array with integers. The number of elements in the array corresponds
        to the number of components. The value of each element corresponds to
        the number of equations for that component. The sum of the elements is
        the total number of variables.

    d_poly : numpy array or int
        An array of integers or a single integer corresponding to the degree of
        the polynomials. If an array is provided then each integer corresponds
        to the degree of each component. If a single integer is provided then
        all the polymonials are based on this degree.

    structure : str or odd int or list
        This controls the Jacobian structure.
        'LT' for lower triangular, 'UT' for upper triangular,
        'LH' for lower Hessenberg, 'UH for upper Hessenberg',
        and an odd integer n for n-diagonal.
        If a list is provided, then each element corresponds to the structure of
        each corresponding component.
        If a single string or integer is provided, it is the structure for the
        entire system.

    tol : number
        Terms in equations with coefficients less than this number will be
        omitted.
        """

    total_n_vars = n_vars.sum() # total no. of variables

    if type(structure) is int: # for n-diag structure, check if input is ok
        assert (structure%2==0 or structure < 1 or (structure+1)/2 > (total_n_vars)) == False , \
        "Error: For n-diagonal matrix input odd number greater than 0 and less than the total no. of vars"

    # ==========================================================================
    # Nonlinearity and coupling functions
    # ==========================================================================
    def fr_func(r):
        """ Definition of nonlinearity function."""
        return 1.5**(-r)
    def g_func(i, Ak):
        """ Definition of coupling function."""
        return 1.5**(-abs(i - Ak))
    # ==========================================================================
    # End of nonlinearity and coupling function definitons
    # ==========================================================================

    # ==========================================================================
    # Function for outputting terms of the equations.
    # ==========================================================================
    def printterms(i, r, rfact, fr, structure, A, tol, total_n_vars):
            """
            Function for outputting the terms of the equations.

            Parameters
            ----------
            i : int
                Equation index.

            r : int
                Degree of term.

            rfact : number
                1/r!

            fr : number
                A coefficient decaying with r that influences nonlinearity.

            structure : str or odd int
                See WriteProb docstring.

            A : numpy array
                An array with the indices of the variables in a term.

            tol : number
                See WriteProb docstring.

            total_n_vars : int
                Total number of variables in the problem.
                """

            if type(structure) is int: # n-diagonal
                A = A - np.ones((r,), dtype=np.int)* ((structure +1)/2 - i)

            elif structure == "UT":
                A = A + np.ones((r,), dtype=np.int)* (i - 1)

            elif structure == "UH":
                A = A + np.ones((r,), dtype=np.int)* (i - 2)

            if (np.min(A) < 1) or (np.max(A) > total_n_vars):
                return

            partial_deriv = rfact * fr * g_func(i, A[0])

            for k in range(1, r): # calculate coeff of term
                partial_deriv *= g_func(i, A[k])

            if partial_deriv < tol: # do not include term if coeff is too small
                return

            for k in range(r): # print one term of Taylor exp
                Ak = A[k]
                if k==0:
                    f.write(textwrap.fill("+ (v%d - %d)*%.15f", initial_indent="\n                         ") %(Ak, Ak, partial_deriv))
                else:
                    f.write(textwrap.fill("* (v%d - %d)", initial_indent="\n                         ") %(Ak, Ak))
    # ==========================================================================
    # End of printterms()
    # ==========================================================================

    with open(file_name, 'w') as f:

        f.write(textwrap.dedent('''\
                from __future__ import print_function
                import numpy as np
                from openmdao.api import Component, Group, NLGaussSeidel, ScipyGMRES, Problem, Newton, DirectSolver
                \n
                    '''))

        n_comps = n_vars.size # no. of components

        indices = np.zeros((n_comps + 1),dtype=np.int)
        for i in xrange(n_comps):
            indices[i] = np.sum(n_vars[0 : i + 1])
        # indices contains an array based on n_vars where the preceding elements
        # of each element is added to the element. Also, a 0 is appended to the
        # array. e.g. if n_vars is [5,10,15] then indices is [5,15,30,0].
        # This is used to determine the incides of the eqs in each component.

        for ci in xrange(1, n_comps + 1): # loop over no. of components
            f.write(textwrap.dedent('''\
                class Dis%d(Component):
                    """Component containing Discipline %d."""

                    def __init__(self):
                        super(Dis%d, self).__init__()

                    ''') %(ci, ci, ci))

            gt = indices[ci - 2] # this component is for eqs with indices greater than this and
            lt = indices[ci - 1] + 1 # less than this

            for i in xrange(1, gt + 1):
                f.write(textwrap.fill("self.add_param('v%d', val=0.)", initial_indent="\n        ") %(i))
            for i in xrange(gt + 1, lt):
                f.write(textwrap.fill("self.add_state('v%d', val=0.)", initial_indent="\n        ") %(i))
            for i in xrange(lt,indices[n_comps - 1] + 1):
                f.write(textwrap.fill("self.add_param('v%d', val=0.)", initial_indent="\n        ") %(i))

            f.write(textwrap.dedent('''\n
                # Use finite differencing to compute derivatives.
                        self.fd_options['force_fd'] = True

                    def solve_nonlinear(self, params, unknowns, resids):
                        self.apply_nonlinear(params, unknowns, resids)

                    def apply_nonlinear(self, params, unknowns, resids):

                    '''))

            for i in xrange(1, gt + 1):
                f.write(textwrap.fill("v%d = params['v%d']", initial_indent="\n        ") %(i, i))
            for i in xrange(gt + 1, lt):
                f.write(textwrap.fill("v%d = unknowns['v%d']", initial_indent="\n        ") %(i, i))
            for i in xrange(lt, indices[n_comps - 1] + 1):
                f.write(textwrap.fill("v%d = params['v%d']", initial_indent="\n        ") %(i, i))

            f.write(textwrap.dedent('''\n'''))

            for i in xrange(gt + 1, lt): # loop over no. of eqs in current component
                f.write(textwrap.fill("resids['v%d'] = ( 0 ", initial_indent="\n        ") %(i))

                if type(d_poly) is int:
                    d = d_poly
                else:
                    d = d_poly[ci - 1]

                if type(structure) is list:
                    struct = structure[ci - 1]
                else:
                    struct = structure

                for r in range(1, d + 1): # for each degree
                    A = np.ones((r,), dtype=np.int) # array for term arguments
                    rfact = 1. / math.factorial(r)
                    fr = fr_func(r)

                    if type(struct) is int: # i.e. n-diagonal

                        for ai1 in xrange(1, struct**r + 1):
                            # output terms of equation using function defined above
                            printterms(i, r, rfact, fr, struct, A, tol, total_n_vars)

                            for ai2 in range(1, r + 1): # update arguments for term
                                if ai1%(struct**(ai2 - 1)) == 0:
                                    index = r - ai2
                                    A[index] = A[index] + 1
                                    if A[index] > struct:
                                        A[index] = 1

                    elif struct == "UT": # i.e. upper-tri

                        for ai1 in xrange(1, (total_n_vars - i + 1)**r + 1):
                            # output terms of equation using function defined above
                            printterms(i, r, rfact, fr, struct, A, tol, total_n_vars)

                            for ai2 in range(1, r + 1): # update arguments for term
                                if ai1%((total_n_vars - i + 1)**(ai2 - 1)) == 0:
                                    index = r - ai2
                                    A[index] = A[index] + 1
                                    if A[index] > (total_n_vars - i + 1):
                                        A[index] = 1

                    elif struct == "LT": # i.e. lower-tri

                        for ai1 in xrange(1, i**r + 1):
                            # output terms of equation using function defined above
                            printterms(i, r, rfact, fr, struct, A, tol, total_n_vars)

                            for ai2 in range(1, r + 1): # update arguments for term
                                if ai1%(i**(ai2 - 1)) == 0:
                                    index = r - ai2
                                    A[index] = A[index] + 1
                                    if A[index] > i:
                                        A[index] = 1

                    elif struct == "UH": # i.e. upper-Hessenberg

                        for ai1 in xrange(1, (total_n_vars - i + 2)**r + 1):
                            # output terms of equation using function defined above
                            printterms(i, r, rfact, fr, struct, A, tol, total_n_vars)

                            for ai2 in range(1, r + 1): # update arguments for term
                                if ai1%((total_n_vars - i + 2)**(ai2 - 1)) == 0:
                                    index = r - ai2
                                    A[index] = A[index] + 1
                                    if A[index] > (total_n_vars - i + 2):
                                        A[index] = 1

                    elif struct == "LH": # i.e. lower-Hessenberg

                        for ai1 in xrange(1, (i+1)**r + 1):
                            # output terms of equation using function defined above
                            printterms(i, r, rfact, fr, struct, A, tol, total_n_vars)

                            for ai2 in range(1, r + 1): # update arguments for term
                                if ai1%((i+1)**(ai2 - 1)) == 0:
                                    index = r - ai2
                                    A[index] = A[index] + 1
                                    if A[index] > (i+1):
                                        A[index] = 1

                    else:

                        for ai1 in xrange(1, total_n_vars**r + 1):
                            # output terms of equation using function defined above
                            printterms(i, r, rfact, fr, struct, A, tol, total_n_vars)

                            for ai2 in range(1, r + 1): # update arguments for term
                                if ai1%(total_n_vars**(ai2 - 1)) == 0:
                                    index = r - ai2
                                    A[index] = A[index] + 1
                                    if A[index] > total_n_vars:
                                        A[index] = 1

                f.write(textwrap.dedent('''\
                    ) \n \n
                        ''') )

        f.write(textwrap.dedent('''\
                class DisGroup(Group):

                    def __init__(self):
                        super(DisGroup, self).__init__()
                    '''))

        for j in xrange(1, n_comps + 1):
            f.write(textwrap.fill("self.add('d%d', Dis%d(), promotes=['*'])", initial_indent="\n        ") %(j, j))

        f.write(textwrap.dedent('''\n
                # Use Newton solver.
                        self.nl_solver = Newton()
                        self.nl_solver.options['atol'] = 1.0e-12
                        self.nl_solver.options['alpha'] = 1.
                        #self.ln_solver = ScipyGMRES()
                        self.ln_solver = DirectSolver()
                        #self.deriv_options['type'] = 'fd'


                top = Problem()
                top.root = DisGroup()
                top.setup() \n
                    '''))

        for j in xrange(1, total_n_vars + 1): # initial guesses
            f.write(textwrap.fill("top['v%d'] = 123.", initial_indent="\n") %(j))

        f.write(textwrap.dedent('''\n
                top.run() \n
                print("Solutions and residuals: ")
                    '''))

        for j in xrange(1, total_n_vars + 1): # command to print solutions
            f.write(textwrap.fill("print('v%d = ', top['v%d'])", initial_indent="\n") %(j, j))

        for j in xrange(1, total_n_vars + 1): # command to print residuals
            f.write(textwrap.fill("print('Resid v%d = ', top.root.resids['v%d'])", initial_indent="\n") %(j, j))

    print 'Execution completed.'
# =============================================================================
# End of WriteProb()
# =============================================================================

if __name__ == "__main__":
# =============================================================================
# INPUT & EXECUTION
# =============================================================================

    n_vars = np.array([10,10,5,10])
    d_poly = np.array([1,2,3,4])
    struct = [1,3,"LH","UT"]

    tol = 5e-16

    WriteProb("comparison2.py", n_vars, d_poly, struct, tol)
