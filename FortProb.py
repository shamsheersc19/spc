# =============================================================================
# External Python modules
# =============================================================================
from __future__ import print_function
import math
import numpy as np
from openmdao.api import Component, Group, NLGaussSeidel, ScipyGMRES, Problem, \
Newton, DirectSolver
try:
    import lib
    fortranflag = True
except:
    fortranflag = False
#fortranflag = False
# ==========================================================================
# Nonlinearity and coupling functions
# ==========================================================================
# Note: If using FORTRAN code, these need to be updated in .f90 file as well.
def fr_func(r):
    """ User defined nonlinearity function."""
    return 1.1**(-r)

def g_func(i, AAk):
    """ User defined coupling function."""
    return 1.1**(-abs(i - AAk))

# ==========================================================================
# Function for calculating the coefficients of a terms in Taylor series
# ==========================================================================

def coefficient(rfact, fr, i, B, r):
    """ Function for calculating coefficients of Taylor terms."""

    partial_deriv = fr * g_func(i, B[0])

    for k in range(1, r): # calculate coeff of term
        partial_deriv *= g_func(i, B[k])

    return rfact*partial_deriv

# =============================================================================
# Function for array of last indices of each component
# =============================================================================

def getlastind(n_vars):
    """
    indices contains an array based on n_vars where the preceding elements
    of each element is added to the element. Also, a 0 is appended to the
    array. e.g. if n_vars is [5,10,15] then indices is [5,15,30,0].
    This is used to determine the incides of the eqs in each component.
    """
    n_comps = n_vars.size # no. of components
    indices = np.zeros((n_comps + 1),dtype=np.int)
    for i in xrange(n_comps):
        indices[i] = np.sum(n_vars[0 : i + 1])

    return indices

# =============================================================================
# Function for list of indices that each equation depends on
# =============================================================================
def PreProcessing(ci, n_vars, last_ind, d_poly, comp_struct, which_comps):
    """
    Returns two np arrays. The first is a 2-d array of the varaible indices that
    the polynomials depend on. Each row corresponds to a polynomial. In this
    array, 0's are just placeholders and do not correspond to a variable index.
    The second 1-d array records the no. of elements in each row that are non-
    zero.

    Parameters
    ----------

    ci : int
        Component index.

    n_vars : numpy array
        An array with integers. The number of elements in the array corresponds
        to the number of components. The value of each element corresponds to
        the number of equations for that component. The sum of the elements is
        the total number of variables.

    last_ind : numpy array
        This is computed by the getlastind() function using n_vars as the input.

    d_poly : numpy array or int
        An array of integers or a single integer corresponding to the degree of
        the polynomials. If an array is provided then each integer corresponds
        to the degree of each component. If a single integer is provided then
        all the polymonials are based on this degree.

    comp_struct : str or odd int or list
        This controls the Jacobian structure of the component block.
        'LT' for lower triangular, 'UT' for upper triangular,
        'LH' for lower Hessenberg, 'UH for upper Hessenberg',
        and an odd integer n for n-diagonal.
        If a list is provided, then each element corresponds to the structure of
        each corresponding component.
        If a single string or integer is provided, it is the structure for all
        the components.

    which_comps: numpy array
        An array containing the component indices this component depends on.

    """

    n_comps = n_vars.size # no. of components
    total_n_vars = n_vars.sum() # total no. of variables

    gt = last_ind[ci - 2] # this component is for eqs with indices greater than this and
    lt = last_ind[ci - 1] + 1 # less than this

    if type(d_poly) is int:
        d_poly = d_poly
    else:
        d_poly = d_poly[ci - 1]

    if type(comp_struct) is list:
        struct = comp_struct[ci - 1]
    else:
        struct = comp_struct

    # Find number of cols for A array
    if type(struct) is int:
        Awidth = struct
    elif struct == "Custom":
        Awidth = 3 # enter width here for Custom
    else:
        Awidth = n_vars[ci - 1]

    for i in xrange(which_comps.size):
        if (which_comps[i] != ci):
            Awidth += n_vars[which_comps[i] - 1]

    A = np.zeros((n_vars[ci - 1], Awidth), dtype=np.int)
    Ai_size = np.zeros(n_vars[ci - 1], dtype=np.int)


    if type(struct) is int: # used for creating A(i) when struct is int
        bwp = (struct + 1)/2
        bwm = (struct - 1)/2

    counter = 0
    for i in xrange(gt + 1, lt):
    # loop over no. of eqs in current component to determine A(i)'s
        if type(struct) is int:
            if counter < bwp:
                Ai = np.arange(gt + 1, i + bwp)
            elif (n_vars[ci - 1] - counter) < bwp:
                Ai = np.arange(i - bwm, lt)
            else:
                Ai = np.arange(i - bwm, i + bwp)

        elif struct == "UT":
            Ai = np.arange(i, lt)

        elif struct == "LT":
            Ai = np.arange(gt + 1, i + 1)

        elif struct == "UH":
            if counter == 0:
                Ai = np.arange(i, lt)
            else:
                Ai = np.arange(i - 1, lt)

        elif struct == "LH":
            if i == lt - 1:
                Ai = np.arange(gt + 1, lt)
            else:
                Ai = np.arange(gt + 1, i + 2)

        elif struct == "Custom":
            Ai = np.array([1, 9, 21]) # create a custom Ai here

        else:
            Ai = np.arange(gt + 1, lt) # fully populated jacobian block

        for j in xrange(which_comps.size):
            if (which_comps[j] != ci):
                for k in xrange(last_ind[which_comps[j] - 2] + 1, last_ind[which_comps[j] - 1] + 1):
                    # if np.random.rand() > 0.95: # adjust this number
                    #     Ai = np.append(Ai, k)
                    # if k%200 == 0:
                    #     Ai = np.append(Ai, k)
                    if k%2 == 0:
                        if i%2 == 0:
                            Ai = np.append(Ai, k)
                        else:
                            Ai = np.append(Ai, k-1)


        Ai_size[counter] = Ai.size

        if Ai.size < Awidth:
            Ai = np.append(Ai, np.zeros(Awidth - Ai.size))

        elif Ai.size > Awidth:
            print("ERORR in PreProcessing")
            exit()

        A[counter] = Ai.copy()
        counter += 1

    return A, Ai_size

# =============================================================================
# Class definition
# =============================================================================
class ScalableComp(Component):
    """
    Scalable component.
    """

    def __init__(self, ci, n_vars, last_ind, d_poly, A, Aisizes, which_comps):
        super(ScalableComp, self).__init__()

        self.ci = ci
        self.n_vars = n_vars
        self.n_comps = n_vars.size # no. of components
        self.total_n_vars = n_vars.sum() # total no. of variables
        self.last_ind = last_ind
        self.gt = self.last_ind[ci - 2] # this component is for eqs with indices greater than this and
        self.lt = self.last_ind[ci - 1] + 1 # less than this

        if type(d_poly) is int:
            self.d_poly = d_poly
        else:
            self.d_poly = d_poly[ci - 1]

        self.A = A
        self.Aisizes = Aisizes
        self.which_comps = which_comps

        for i in xrange(1, self.which_comps.size + 1):
            if self.which_comps[i - 1] == ci:
                self.add_state('v'+str(which_comps[i - 1]), val=np.zeros(n_vars[self.which_comps[i - 1] - 1]))
            else:
                self.add_param('v'+str(which_comps[i - 1]), val=np.zeros(n_vars[self.which_comps[i - 1] - 1]))

        if self.which_comps.size == 1: # if there are no params, add some
            try:
                self.add_param('v'+str(self.ci + 1), val=np.zeros(n_vars[self.ci]))
            except:
                self.add_param('v'+str(self.ci - 1), val=np.zeros(n_vars[self.ci - 2]))

        #self.deriv_options['type'] = 'fd'
        self.resids_count = 0
        self.par_deriv_count = 0
    def solve_nonlinear(self, params, unknowns, resids):
        self.apply_nonlinear(params, unknowns, resids)

    def apply_nonlinear(self, params, unknowns, resids):
        self.resids_count += 1
        print(self.ci, self.resids_count, "resid")
        v = np.empty(0)
        for i in xrange(1, self.n_comps + 1):
            if i == self.ci:
                v = np.append(v, unknowns['v'+str(i)])
            else:
                try:
                    v = np.append(v, params['v'+str(i)])
                except:
                    v = np.append(v, np.zeros(self.n_vars[i - 1]))


        Awidth = self.A.shape[1]

        counter = 0
        if fortranflag:
            residuals = lib.returnres(self.gt, self.lt, self.d_poly, self.A,\
                 Awidth, self.Aisizes, v, self.total_n_vars)

            resids['v'+str(self.ci)] = residuals

        else:
            residuals = np.empty(self.n_vars[self.ci - 1])
            for i in xrange(self.gt + 1, self.lt):
                Ai = self.A[counter]
                size = self.Aisizes[counter]
                res = 0
                for r in range(1, self.d_poly + 1): # for each degree

                    AA = np.ones(r, dtype=np.int) # array for term arguments
                    B = np.ones(r, dtype=np.int) # array for mapped term arguments
                    rfact = 1. / math.factorial(r)
                    fr = fr_func(r)

                    for ai1 in xrange(1, size**r + 1):

                        for k in range(r): # Mapping
                            B[k] = Ai[AA[k] - 1]

                        product = coefficient(rfact, fr, i, B, r)

                        for k in range(r): # one term of Taylor exp
                            Bk = B[k]
                            product *= (v[Bk - 1] - Bk)

                        res += product

                        for ai2 in range(r): # update arguments for term
                            if ai1%(size**ai2) == 0:
                                index = r - ai2 - 1
                                AA[index] += 1
                                if AA[index] > size:
                                    AA[index] = 1
                residuals[counter] = res
                counter += 1
            resids['v'+str(self.ci)] = residuals
            residuals = None

    def linearize(self, params, unknowns, resids):
        """Analytical derivatives."""
        self.par_deriv_count += 1
        print(self.ci, self.resids_count, "par der")
        v = np.empty(0)
        for i in xrange(1, self.n_comps + 1):
            if i == self.ci:
                v = np.append(v, unknowns['v'+str(i)])
            else:
                try:
                    v = np.append(v, params['v'+str(i)])
                except:
                    v = np.append(v, np.zeros(self.n_vars[i - 1]))

        Awidth = self.A.shape[1]
        counter = 0
        J = {}

        if fortranflag:

            pd = lib.returnpder(self.gt, self.lt, self.d_poly, self.A,\
                     Awidth, self.Aisizes, v, self.total_n_vars)

            for i in xrange(self.which_comps.size):
               J[('v'+str(self.ci), 'v'+str(self.which_comps[i]))] = \
               pd[:, self.last_ind[self.which_comps[i]-2]:self.last_ind[self.which_comps[i]-1] ]

        else:
            pd = np.zeros((self.n_vars[self.ci - 1], self.total_n_vars))
            for i in xrange(self.gt + 1, self.lt):
                Ai = self.A[counter]
                size = self.Aisizes[counter]
                for j in xrange(size):
                    p = Ai[j]
                    par_deriv = 0
                    for r in range(1, self.d_poly + 1): # for each degree

                        AA = np.ones(r, dtype=np.int) # array for term arguments
                        B = np.ones(r, dtype=np.int) # array for mapped term arguments
                        rfact = 1. / math.factorial(r)
                        fr = fr_func(r)

                        for ai1 in xrange(1, size**r + 1):

                            for k in range(r): # Mapping
                                B[k] = Ai[AA[k] - 1]

                            occurance = np.in1d(B, p).sum()

                            if occurance > 0:

                                product = coefficient(rfact, fr, i, B, r)
                                occ_switch = True
                                for k in range(r): # one term of Taylor exp
                                    Bk = B[k]
                                    if occ_switch and Bk == p:
                                        product *= occurance
                                        occ_switch = False
                                    else:
                                        product *= (v[Bk - 1] - Bk)

                                par_deriv += product

                            for ai2 in range(r): # update arguments for term
                                if ai1%(size**ai2) == 0:
                                    index = r - ai2 - 1
                                    AA[index] += 1
                                    if AA[index] > size:
                                        AA[index] = 1

                    pd[counter, p - 1] = par_deriv
                counter += 1

            for i in xrange(self.which_comps.size):
               J[('v'+str(self.ci), 'v'+str(self.which_comps[i]))] = \
               pd[:, self.last_ind[self.which_comps[i]-2]:self.last_ind[self.which_comps[i]-1] ]
            pd = None
        return J

# =============================================================================
# Group
# =============================================================================

class DisGroup(Group):

    def __init__(self, n_vars, last_ind, d_poly, Alist, Aisizelist, comp_range):
        super(DisGroup, self).__init__()

        for ci in xrange(comp_range[0], comp_range[1] + 1):
            self.add('dis'+str(ci), ScalableComp(ci, n_vars, last_ind, d_poly,
            Alist[ci - 1], Aisizelist[ci - 1], which_comps_list[ci - 1]), promotes=['*'])

        self.nl_solver = Newton()
        self.nl_solver.options['atol'] = 1.0e-12
        self.nl_solver.options['alpha'] = 1.
        #self.ln_solver = ScipyGMRES()
        self.ln_solver = DirectSolver()

class ProbGroup(Group):

    def __init__(self):
        super(ProbGroup, self).__init__()

        self.nl_solver = Newton()
        self.nl_solver.options['atol'] = 1.0e-12
        self.nl_solver.options['alpha'] = 1.
        #self.ln_solver = ScipyGMRES()
        self.ln_solver = DirectSolver()

# =============================================================================
# Input and Execution
# =============================================================================
if __name__ == "__main__":

    n_vars = 10*np.ones(10000)
    d_poly = 1
    comp_struct = 5

    providing_DSM = False

    if providing_DSM: # provide DSM here
        DSM_T = np.array(([1,1,0,0,0,0,0,0,0,0],
                          [1,1,1,0,0,0,0,0,0,0],
                          [0,1,1,1,0,0,0,0,0,0],
                          [0,0,1,1,1,0,0,0,0,0],
                          [0,0,0,1,1,1,0,0,0,0],
                          [0,0,0,0,1,1,1,0,0,0],
                          [0,0,0,0,0,1,1,1,0,0],
                          [0,0,0,0,0,0,1,1,1,0],
                          [0,0,0,0,0,0,0,1,1,1],
                          [0,0,0,0,0,0,0,0,1,1]))

        # take DSM_T and store info in memory efficient list
        which_comps_list = []
        for i in xrange(DSM_T.shape[0]):
            which_comps_array = np.empty(0, dtype=np.int)
            for j in xrange(DSM_T.shape[0]):
                if DSM_T[i,j]:
                    which_comps_array = np.append(which_comps_array, j+1)
            which_comps_list.append(which_comps_array)

    else: # create which_comps_list here
        which_comps_list = []

        for i in range(1, n_vars.size + 1):
            which_comps_list.append(np.arange(i - 1, i + 2))
        which_comps_list[0] = np.array([1,2])
        which_comps_list[n_vars.size - 1] = np.array([n_vars.size - 1, n_vars.size ])

    last_ind = getlastind(n_vars)
    Alist = []
    Aisizelist = []

    for ci in xrange(1, n_vars.size + 1):
        AL, AS = PreProcessing(ci, n_vars, last_ind, d_poly, comp_struct, which_comps_list[ci - 1])
        Alist.append(AL)
        Aisizelist.append(AS)
    print("PREPROC FINISHED")
    top = Problem()
    top.root = DisGroup(n_vars, last_ind, d_poly, Alist, Aisizelist, [1, n_vars.size])
    top.setup()

    # top = Problem()
    # G1 = DisGroup(n_vars, d_poly, Alist, Aisizelist, [1,4])
    # G2 = DisGroup(n_vars, d_poly, Alist, Aisizelist, [5,8])
    # top.root = ProbGroup()
    # top.root.add('g1', G1, promotes=['*'])
    # top.root.add('g2', G2, promotes=['*'])
    # top.setup()


    for i in xrange(1, n_vars.size + 1):
        top['v'+str(i)] = 123. * np.ones(n_vars[i-1])

    top.run()

    print("Solutions:\n")
    for i in xrange(1, n_vars.size+1):
        print('v'+str(i), ' = ', top['v'+str(i)])
    print("\nResiduals:\n")
    for i in xrange(1, n_vars.size+1):
        print('Resids v'+str(i), ' = ', top.root.resids['v'+str(i)])
    print("\nNo. of times resids are evaluated:\n")
    for i in xrange(1, n_vars.size + 1):
        print("component",i, eval("top.root.dis%d.resids_count" %i))
    print("\nNo. of times partial derivs are evaluated:\n")
    for i in xrange(1, n_vars.size + 1):
        print("component",i, eval("top.root.dis%d.par_deriv_count" %i))
