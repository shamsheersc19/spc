import numpy as np

# =============================================================================
# PLOT COMPONENT LEVEL JACOBIAN
# =============================================================================
def print_jaco():
    jaco = np.zeros((n_vars.size, n_vars.size))
    for i in xrange(n_vars.size):
        try:
            for j in which_comps_list[i]:
                jaco[i][j - 1] = 1
        except:
            jaco[i][i] = 1
    import matplotlib.pyplot as plt
    plt.spy(jaco)
    plt.show()

# =============================================================================
# MAIN CODE
# =============================================================================

def add_hier_info(group_name, group_type, what, DSMT=None):

    add_hier_dict[group_name] = what
    add_range_dict[group_name] = what

    if len(what) == 1: #for single component groups
        which_comps_list[what[0] - 1] = np.array([what[0]])
        add_hier_dict[group_name] = [what[0], what[0]]
        add_range_dict[group_name] = [what[0], what[0]]
        return

    if group_type == "CG": # for component groups
        for i in xrange(what[0], what[1] + 1):
            which_comps_temp = []
            for j in xrange(what[0], what[1] + 1):
                if DSMT[i - what[0]][j - what[0]] == 1:
                    which_comps_temp.append(j)
            which_comps_list[i - 1] = np.append(which_comps_list[i - 1],
                                           np.asarray(which_comps_temp))

    elif group_type == "GG": #for groups of groups
        lower = 1e29; upper = 0
        for i in xrange(len(what)):
            lower = min(min(add_range_dict[what[i]]), lower)
            upper = max(max(add_range_dict[what[i]]), upper)
        add_range_dict[group_name] = [lower, upper]

        for i in xrange(DSMT.shape[0]):
            for j in xrange(DSMT.shape[0]):
                if i != j and DSMT[i][j] != 0:
                    block_rows = [add_range_dict[what[i]][0], add_range_dict[what[i]][1]]
                    block_cols = [add_range_dict[what[j]][0], add_range_dict[what[j]][1]]

                    if DSMT[i][j] == 1: # Option 1: Random probability
                        prob = 0.6 # adjust probability here

                        for k in xrange(block_rows[0], block_rows[1] + 1):
                            which_comps_temp = []
                            for l in xrange(block_cols[0], block_cols[1] + 1):
                                if np.random.rand() < prob:
                                    which_comps_temp.append(l)
                            which_comps_list[k - 1] = np.append(which_comps_list[k - 1],
                                                           np.asarray(which_comps_temp, dtype = int))

                    elif DSMT[i][j] == 2: # Option 2: n-diag w/ spacing
                        n_diag = 7
                        spacing = 3

                        half_bw = (n_diag - 1) / 2
                        counter = 0
                        for k in xrange(block_rows[0], block_rows[1] + 1):
                            if counter < half_bw:
                                which_comps_temp = np.arange(block_cols[0] + counter - half_bw,
                                                             block_cols[0] + counter + half_bw + 1,
                                                             spacing, dtype = int)

                                check = True
                                while check:
                                    if which_comps_temp.min() < block_cols[0]:
                                        which_comps_temp = np.delete(which_comps_temp, 0)
                                    else:
                                        check = False

                            elif counter < block_cols[1] - block_cols[0] - half_bw + 1:
                                which_comps_temp = np.arange(block_cols[0] + counter - half_bw,
                                                             block_cols[0] + counter + half_bw + 1,
                                                             spacing, dtype = int)

                            elif counter < block_cols[1] - block_cols[0] + half_bw + 1:
                                which_comps_temp = np.arange(block_cols[0] + counter - half_bw,
                                                             block_cols[0] + counter + half_bw + 1,
                                                             spacing, dtype = int)

                                check = True
                                while check:
                                    if np.max(which_comps_temp) > block_cols[1]:
                                        which_comps_temp = np.delete(which_comps_temp, -1)
                                    else:
                                        check = False
                            else:
                                which_comps_temp = np.empty(0, dtype = int)

                            which_comps_list[k - 1] = np.append(which_comps_list[k - 1],
                                                           which_comps_temp)
                            counter += 1

# =============================================================================
# Input and Execution
# =============================================================================
n_vars = 10*np.ones(25)
add_hier_dict = {}
add_range_dict = {}
which_comps_list = []
for i in xrange(n_vars.size):
    which_comps_list.append(np.empty(0, dtype = np.int))

# Groups with components
add_hier_info("G1", "CG", [1], np.array(([1])))
#print_jaco()
add_hier_info("G2", "CG", [2,6], np.array(([1,1,1,1,1],
                                           [0,1,1,1,1],
                                           [0,0,1,1,1],
                                           [0,0,0,1,1],
                                           [0,0,0,0,1])))
#print_jaco()
add_hier_info("G3", "CG", [7,10], np.array(([1,1,0,0],
                                            [1,1,1,0],
                                            [0,1,1,1],
                                            [0,0,1,1])))
#print_jaco()
add_hier_info("G4", "CG", [11,15], np.array(([1,1,1,1,1],
                                             [0,1,1,1,1],
                                             [0,0,1,1,1],
                                             [0,0,0,1,1],
                                             [0,0,0,0,1])))
#print_jaco()
add_hier_info("G5", "CG", [16,19], np.array(([1,1,0,0],
                                             [1,1,1,0],
                                             [0,1,1,1],
                                             [0,0,1,1])))
#print_jaco()
add_hier_info("G6", "CG", [20,22], np.array(([1,0,0],
                                             [1,1,0],
                                             [1,1,1])))
#print_jaco()
add_hier_info("G7", "CG", [23,25], np.array(([1,0,0],
                                             [1,1,0],
                                             [1,1,1])))

#Groups with groups
print_jaco()
add_hier_info("G8", "GG", ["G2", "G3"], np.array(([1,2],
                                                  [2,1])))
print_jaco()
add_hier_info("G9", "GG", ["G4", "G5"], np.array(([1,2],
                                                  [2,1])))
print_jaco()
add_hier_info("G10", "GG", ["G6", "G7"], np.array(([1,0],
                                                   [2,1])))
print_jaco()
# add_hier_info("G11", "GG", ["G8", "G9"], np.array(([1,0],
#                                                    [1,1])))
# print_jaco()
# add_hier_info("G12", "GG", ["G11", "G10"], np.array(([1,1],
#                                                      [0,1])))
add_hier_info("G11", "GG", ["G8", "G9", "G10"], np.array(([1,2,2],
                                                          [2,1,2],
                                                          [0,0,1])))
print_jaco()
add_hier_info("G13", "GG", ["G1", "G11"], np.array(([1,0],
                                                    [1,1])))
print_jaco()
print which_comps_list
