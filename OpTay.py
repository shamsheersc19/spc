# =============================================================================
# Python modules
# =============================================================================
from __future__ import print_function
import math
import numpy as np
import sys
from openmdao.api import Component, IndepVarComp, ExecComp, Group, NLGaussSeidel, ScipyGMRES, Problem, \
Newton, DirectSolver, ScipyOptimizer, SqliteRecorder, pyOptSparseDriver
try:
    import lib
    fortranflag = True
except:
    fortranflag = False
#fortranflag = False
# ==========================================================================
# Nonlinearity and coupling functions
# ==========================================================================
# Note: If using FORTRAN code, these need to be updated in .f90 file as well.
def fr_func(r):
    """ User defined nonlinearity function."""
    return 1.1**(-r)

def g_func(i, jk):
    """ User defined coupling function."""
    return 1.0001**(-abs(i - jk))

# ==========================================================================
# Function for calculating the coefficients of a term in the Taylor series
# ==========================================================================

def coefficient(rfact, fr, i, j, r):
    """ Function for calculating coefficients of Taylor terms."""

    partial_deriv = fr * g_func(i, j[0])

    for k in range(1, r): # calculate coeff of term
        partial_deriv *= g_func(i, j[k])

    return rfact*partial_deriv

# =============================================================================
# Function for array of last indices of each component
# =============================================================================

def getlastind(n_vars):
    """
    indices contains an array based on n_vars where the preceding elements
    of each element is added to the element. Also, a 0 is appended to the
    array. e.g. if n_vars is [5,10,15] then indices is [5,15,30,0].
    This is used to determine the incides of the eqs in each component.
    """
    n_comps = n_vars.size # no. of components
    indices = np.zeros((n_comps + 1),dtype=np.int)
    for i in xrange(n_comps):
        indices[i] = np.sum(n_vars[0 : i + 1])

    return indices

# =============================================================================
# Function that generates required which_comps_list from DSM_T
# =============================================================================

def create_which_comps_list_from_DSM_T(DSM_T):
    """ Function for creating which_comps_list from DSM_T. """

    which_comps_list = []
    for i in xrange(DSM_T.shape[0]):
        which_comps_array = np.empty(0, dtype=np.int)
        for j in xrange(DSM_T.shape[0]):
            if DSM_T[i,j]:
                which_comps_array = np.append(which_comps_array, j+1)
        which_comps_list.append(which_comps_array)
    return which_comps_list

# =============================================================================
# Function that interprets the user input hierarchy info
# =============================================================================

def add_hier_info(dhl, group_name, group_type, what, DSMT=np.array(([1]))):
    """ Function used for describing heirarchical problem structure.
    Parameters
    ----------

    group_name : str
        Group name in format "G#". e.g. "G1" or "G2".

    group_type : str
        "CG" for a group that only contains components.
        "GG" for a group that only contains other groups.

    what : list of int or str
        For a group containing components ("CG") provide a list that contains
        the range of components that it contains. For example, if the group
        contains the first 4 components, input [1,4].
        For a group containing other groups ("GG") provide a list with all the
        names of the groups that it contains. For example, if the group contians
        groups G1, G2, and G7, enter ["G1", "G2", "G7"].The order matters, use
        the order in which the groups appear along the Jacobian diagonal.

    DSM_T : str or numpy array
        To describe the structure, a str or a square numpy array can be provided.
        'LT' for lower triangular, 'UT' for upper triangular,
        'LH' for lower Hessenberg, 'UH' for upper Hessenberg'.
        The square array should contain 1's and 0's to represent the Jacobian
        structure.
        If specifying the structure for a "GG", numbers can be provided for the
        off-diagonal elements of the square array to specify further options for
        the off-diagonal inter-group blocks. The digit before the decimal
        specifies the option number.
        Option 1: Specify probability. The numbers after the decimal specify the
        probability.
        Option 2: Diagonal structure with spacing. The first number after the
        decimal specifies the bandwidth. The second number specifies the spacing
        between the filled-in diagonals.

    """
    add_hier_dict = dhl[0]
    add_range_dict = dhl[1]
    which_comps_list = dhl[2]

    add_hier_dict[group_name] = what
    add_range_dict[group_name] = what

    if len(what) == 1: #for single component groups
        which_comps_list[what[0] - 1] = np.array([what[0]])
        add_hier_dict[group_name] = [what[0], what[0]]
        add_range_dict[group_name] = [what[0], what[0]]
        return

    if group_type == "CG": # for component groups

        if type(DSMT) == str:
            if DSMT == "UT":
                DSMT = np.triu(np.ones((what[-1] - what[0] + 1, what[-1] - what[0] + 1), dtype = int))
            elif DSMT == "LT":
                DSMT = np.tril(np.ones((what[-1] - what[0] + 1, what[-1] - what[0] + 1), dtype = int))
            elif DSMT == "UH":
                DSMT = np.triu(np.ones((what[-1] - what[0] + 1, what[-1] - what[0] + 1), dtype = int))
                for i in xrange(what[-1] - what[0]): DSMT[i+1][i] = 1
            elif DSMT == "LH":
                DSMT = np.tril(np.ones((what[-1] - what[0] + 1, what[-1] - what[0] + 1), dtype = int))
                for i in xrange(what[-1] - what[0]): DSMT[i][i+1] = 1
            elif DSMT == None:
                DSMT = np.ones((what[-1] - what[0] + 1, what[-1] - what[0] + 1), dtype = int)
            else:
                print("Invalid structure for CG."); exit()

        for i in xrange(what[0], what[1] + 1):
            which_comps_temp = []
            for j in xrange(what[0], what[1] + 1):
                if DSMT[i - what[0]][j - what[0]] == 1:
                    which_comps_temp.append(j)
            which_comps_list[i - 1] = np.append(which_comps_list[i - 1],
                                           np.asarray(which_comps_temp, dtype=int))

    elif group_type == "GG": #for groups of groups

        if type(DSMT) == str:
            if DSMT == "UT":
                DSMT = np.triu(np.ones((len(what), len(what)), dtype = int))
            elif DSMT == "LT":
                DSMT = np.tril(np.ones((len(what), len(what)), dtype = int))
            elif DSMT == "UH":
                DSMT = np.triu(np.ones((len(what), len(what)), dtype = int))
                for i in xrange(len(what) - 1): DSMT[i+1][i] = 1
            elif DSMT == "LH":
                DSMT = np.tril(np.ones((len(what), len(what)), dtype = int))
                for i in xrange(len(what) - 1): DSMT[i][i+1] = 1
            elif DSMT == None:
                DSMT = np.ones((len(what), len(what)), dtype = int)
            else:
                print("Invalid structure for GG."); exit()

        lower = 1e29; upper = 0
        for i in xrange(len(what)):
            lower = min(min(add_range_dict[what[i]]), lower)
            upper = max(max(add_range_dict[what[i]]), upper)
        add_range_dict[group_name] = [lower, upper]

        for i in xrange(DSMT.shape[0]):
            for j in xrange(DSMT.shape[0]):
                if i != j and DSMT[i][j] != 0:
                    block_rows = [add_range_dict[what[i]][0], add_range_dict[what[i]][1]]
                    block_cols = [add_range_dict[what[j]][0], add_range_dict[what[j]][1]]

                    if DSMT[i][j] < 1: # check for invalid option
                        print("Error: invalid structure option. Cannot be < 1."); exit()

                    elif DSMT[i][j] < 2: # Option 1: Random probability
                        if DSMT[i][j] == 1:
                            prob = 1. # adjust probability here

                        else:
                            prob = DSMT[i][j] - 1

                        for k in xrange(block_rows[0], block_rows[1] + 1):
                            which_comps_temp = []
                            for l in xrange(block_cols[0], block_cols[1] + 1):
                                if np.random.rand() < prob:
                                    which_comps_temp.append(l)
                            which_comps_list[k - 1] = np.append(which_comps_list[k - 1],
                                                           np.asarray(which_comps_temp, dtype = int))


                    elif DSMT[i][j] < 3: # Option 2: n-diag w/ spacing
                        n_diag = int(DSMT[i][j] * 10 - 20)
                        step = int(round(DSMT[i][j]*100)%10) + 1

                        assert n_diag%2 != 0, "Error: Specified bandwidths should be odd."

                        half_bw = (n_diag - 1) / 2
                        counter = 0
                        for k in xrange(block_rows[0], block_rows[1] + 1):
                            if counter < block_cols[1] - block_cols[0] + half_bw + 1:
                                which_comps_temp = np.arange(block_cols[0] + counter - half_bw,
                                                             block_cols[0] + counter + half_bw + 1,
                                                             step, dtype = int)

                                for check in xrange(which_comps_temp.size):
                                    if which_comps_temp[0] < block_cols[0]:
                                        which_comps_temp = np.delete(which_comps_temp, 0)
                                    elif which_comps_temp[-1] > block_cols[1]:
                                        which_comps_temp = np.delete(which_comps_temp, -1)
                                    else:
                                        break
                            else:
                                which_comps_temp = np.empty(0, dtype = int)

                            which_comps_list[k - 1] = np.append(which_comps_list[k - 1],
                                                           which_comps_temp)
                            counter += 1

                    else: # check for invalid option
                        print("Error: invalid structure. Float must be < 3."); exit()

# =============================================================================
# PREVIEW INTER-COMPONENT JACOBIAN
# =============================================================================

def plot_comp_jaco():
    """ Function for plotting the inter-component Jacobian structure. """

    jaco = np.zeros((n_vars.size, n_vars.size))
    for i in xrange(n_vars.size):
        try:
            for j in which_comps_list[i]:
                jaco[i][j - 1] = 1
        except:
            jaco[i][i] = 1
    import matplotlib.pyplot as plt
    plt.spy(jaco)
    plt.show()

# =============================================================================
# Function for list of indices that each equation depends on
# =============================================================================

def PreProcessing(ci, n_vars, last_ind, d_poly, comp_struct, which_comps, off_diag_comp_struct):
    """
    Returns two np arrays. The first is a 2-D array of the varaible indices that
    the polynomials depend on. Each row corresponds to a polynomial. In this
    array, 0's are just placeholders and do not correspond to a variable index.
    The second 1-D array records the no. of elements in each row that are non-
    zero.

    Parameters
    ----------

    ci : int
        Component index.

    n_vars : numpy array
        A 1-D array with integers. The number of elements in the array corresponds
        to the number of components. The value of each element corresponds to
        the number of equations for that component. The sum of the elements is
        the total number of variables.

    last_ind : numpy array
        This is computed by the getlastind() function using n_vars as the input.

    d_poly : numpy array or int
        A 1-D array of integers or a single integer corresponding to the degree
        of the polynomials. If an array is provided then each integer corresponds
        to the degree of each component. If a single integer is provided then
        all the polymonials are based on this degree.

    comp_struct : str or odd int or list
        This controls the Jacobian structure of the component block.
        'LT' for lower triangular, 'UT' for upper triangular,
        'LH' for lower Hessenberg, 'UH' for upper Hessenberg',
        and an odd integer n for n-diagonal.
        If a list is provided, then each element corresponds to the structure of
        each corresponding component.
        If a single string or integer is provided, it is the structure for all
        the components.

    which_comps: numpy array
        A 1-D array containing the component indices this component depends on.

    off_diag_comp_struct: float or list or numpy array
        A 1-D array containing floats describing the off diagonal component
        level block internal structures. The digit before the decimal
        specifies the option number.
        Option 1: Specify probability. The numbers after the decimal specify the
        probability.
        Option 2: Diagonal structure with spacing. The first number after the
        decimal specifies the bandwidth. The second number specifies the spacing
        between the filled-in diagonals.
        If a single float is provided, it is for the off-diagonals of all the
        components.

    """

    n_comps = n_vars.size # no. of components
    total_n_vars = n_vars.sum() # total no. of variables

    gt = last_ind[ci - 2] # this component is for eqs with indices greater than this and
    lt = last_ind[ci - 1] + 1 # less than this

    if type(d_poly) is int:
        d_poly = d_poly
    else:
        d_poly = d_poly[ci - 1]
        assert d_poly%1 == 0, "Invalid d_poly. Use integers."

    if type(comp_struct) is list:
        struct = comp_struct[ci - 1]
    else:
        struct = comp_struct

    if type(off_diag_comp_struct) is float or type(off_diag_comp_struct) is int:
        off_diag_struct = off_diag_comp_struct
    else:
        off_diag_struct = off_diag_comp_struct[ci - 1]
    assert type(off_diag_struct) is float or type(off_diag_struct) is int, \
    "Invalid off_diag_comp_struct."

    # Find number of cols for A array
    if type(struct) is int:
        Awidth = struct
    elif struct == "C":
        Awidth = 0
    elif struct == "Custom":
        Awidth = 3 # enter width here for Custom
    else:
        Awidth = n_vars[ci - 1]

    for i in xrange(which_comps.size):
        if (which_comps[i] != ci):

            if off_diag_struct < 1:
                print("Invalid off_diag_comp_struct"); exit()

            elif off_diag_struct < 2:
                if off_diag_struct == 1:
                    prob = 1. # adjust probability here
                else:
                    prob = off_diag_struct - 1
                Awidth += min(n_vars[which_comps[i] - 1],
                              int(round(n_vars[which_comps[i] - 1]*prob*1.5)))

            elif off_diag_struct < 3: # n-diag option for off-diagonals
                n_diag = int(off_diag_struct * 10 - 20)
                assert n_diag%2 != 0, "Error: Specified bandwidths should be odd."
                Awidth += n_diag

            else:
                print("Invalid off_diag_comp_struct"); exit()

    A = np.zeros((n_vars[ci - 1], Awidth), dtype=np.int)
    Ai_size = np.zeros(n_vars[ci - 1], dtype=np.int)


    if type(struct) is int: # used for creating A(i) when struct is int
        assert struct%2 !=0, "Invalid comp_struct. Bandwidth should be odd."
        bwp = (struct + 1)/2
        bwm = (struct - 1)/2

    counter = 0
    for i in xrange(gt + 1, lt):
    # loop over no. of eqs in current component to determine A(i)'s
        if type(struct) is int:
            if counter < bwp:
                Ai = np.arange(gt + 1, i + bwp)
            elif (n_vars[ci - 1] - counter) < bwp:
                Ai = np.arange(i - bwm, lt)
            else:
                Ai = np.arange(i - bwm, i + bwp)

        elif struct == "UT":
            Ai = np.arange(i, lt)

        elif struct == "LT":
            Ai = np.arange(gt + 1, i + 1)

        elif struct == "UH":
            if counter == 0:
                Ai = np.arange(i, lt)
            else:
                Ai = np.arange(i - 1, lt)

        elif struct == "LH":
            if i == lt - 1:
                Ai = np.arange(gt + 1, lt)
            else:
                Ai = np.arange(gt + 1, i + 2)

        elif struct == "C":
            Ai = np.array([], dtype=int)

        elif struct == "Custom":
            Ai = np.array([1, 9, 21]) # create a custom Ai here

        elif struct == None:
            Ai = np.arange(gt + 1, lt) # fully populated jacobian block

        else:
            print("invalid comp_struct"); exit()

        for j in xrange(which_comps.size): # indices from other components
            if (which_comps[j] != ci):

                if off_diag_struct < 2:
                    if off_diag_struct == 1:
                        prob = 1. # adjust probability here

                    else:
                        prob = off_diag_struct - 1

                    for k in xrange(last_ind[which_comps[j] - 2] + 1, last_ind[which_comps[j] - 1] + 1):
                        if np.random.rand() < prob: # adjust this number
                            Ai = np.append(Ai, k)

                elif off_diag_struct < 3: # n-diag option for off-diagonals
                    n_diag = int(off_diag_struct * 10 - 20)
                    step = int(round(off_diag_struct*100)%10) + 1

                    half_bw = (n_diag - 1) / 2
                    start = last_ind[which_comps[j] - 2] + 1
                    fin = last_ind[which_comps[j] - 1]
                    if counter < fin - start + half_bw + 2:
                        Ai_temp = np.arange(start + counter - half_bw,
                                            start + counter + half_bw + 1,
                                            step, dtype = int)
                        for check in xrange(Ai_temp.size):
                            if Ai_temp[0] < start:
                                Ai_temp = np.delete(Ai_temp, 0)
                            elif Ai_temp[-1] > fin:
                                Ai_temp = np.delete(Ai_temp, -1)
                            else:
                                break
                        Ai = np.append(Ai, Ai_temp)

        if Ai.size < Awidth: # add 0's to fit in numpy array
            Ai_size[counter] = Ai.size
            Ai = np.append(Ai, np.zeros(Awidth - Ai.size))

        elif Ai.size > Awidth:
            if off_diag_struct < 2:
                Ai = Ai[0:Awidth] # limit if larger than Awidth
                Ai_size[counter] = Awidth
            else:
                print("ERORR in PreProcessing"); exit()

        else:
            Ai_size[counter] = Awidth

        A[counter] = Ai.copy()
        counter += 1

    return A, Ai_size

# =============================================================================
# Run pre-processing step
# =============================================================================

def run_pre_proc(n_vars, last_ind, d_poly, comp_struct, which_comps_list, off_diag_comp_struct):

    Alist = []
    Aisizelist = []
    for ci in xrange(1, n_vars.size + 1):
        AL, AS = PreProcessing(ci, n_vars, last_ind, d_poly, comp_struct, which_comps_list[ci - 1], off_diag_comp_struct)
        Alist.append(AL)
        Aisizelist.append(AS)
    print("PREPROC FINISHED")
    sys.stdout.flush()
    return Alist, Aisizelist

# =============================================================================
# Class definition
# =============================================================================

class ScalableComp(Component):
    """
    Scalable component.
    """

    def __init__(self, ci, n_vars, last_ind, d_poly, A, Aisizes, which_comps):
        super(ScalableComp, self).__init__()

        self.ci = ci # component index
        self.n_vars = n_vars
        self.n_comps = n_vars.size # no. of components
        self.total_n_vars = n_vars.sum() # total no. of variables
        self.last_ind = last_ind
        self.gt = self.last_ind[ci - 2] # this component is for eqs with indices greater than this and
        self.lt = self.last_ind[ci - 1] + 1 # less than this

        if type(d_poly) is int:
            self.d_poly = d_poly
        else:
            self.d_poly = d_poly[ci - 1]

        self.A = A
        self.Aisizes = Aisizes
        self.which_comps = np.sort(which_comps)
        self.cols_req_for_pd = sum([n_vars[i - 1] for i in which_comps]) # used later for analytical partial deriv calcs

        if fortranflag:
            self.full_before, self.empty_before = lib.befores(self.n_vars,
                    self.n_comps, self.which_comps, self.which_comps.size)
            # used later for analytical partial deriv calcs
        else:
            self.empty_before = np.zeros(self.which_comps.size, dtype=int) # used later for analytical partial deriv calcs
            self.full_before = np.zeros(self.which_comps.size, dtype=int) # used later for analytical partial deriv calcs
    	    for i in xrange(self.which_comps.size):
    	        for j in xrange(self.which_comps[i] - 1):
                    if np.any(self.which_comps == j + 1):
                        self.full_before[i] += n_vars[j]
                    else:
    			        self.empty_before[i] += n_vars[j]

        for i in xrange(1, self.which_comps.size + 1):
            if self.which_comps[i - 1] == self.ci:
                self.add_state('v'+str(self.ci), val=np.zeros(n_vars[self.ci - 1]))
            else:
                self.add_param('v'+str(self.which_comps[i - 1]), val=np.zeros(n_vars[self.which_comps[i - 1] - 1]))

        if self.which_comps.size == 1: # if there are no params, add some
            try:
                self.add_param('v'+str(self.ci + 1), val=np.zeros(n_vars[self.ci]))
            except:
                self.add_param('v'+str(self.ci - 1), val=np.zeros(n_vars[self.ci - 2]))

        #self.deriv_options['type'] = 'fd'
        self.resids_count = 0 # for counting residual evaluation calls
        self.par_deriv_count = 0 # for counting par deriv evaluation calls
    def solve_nonlinear(self, params, unknowns, resids):
        pass
    def apply_nonlinear(self, params, unknowns, resids):
        self.resids_count += 1
        print(self.ci, self.resids_count, "resid"); sys.stdout.flush()
        v = np.empty(0)
        for i in xrange(1, self.n_comps + 1):
            if i == self.ci:
                v = np.append(v, unknowns['v'+str(i)])
            else:
                try:
                    v = np.append(v, params['v'+str(i)])
                except:
                    v = np.append(v, np.zeros(self.n_vars[i - 1]))


        Awidth = self.A.shape[1]

        counter = 0
        if fortranflag:
            residuals = lib.returnres(self.gt, self.lt, self.d_poly, self.A,\
                 Awidth, self.Aisizes, v, self.total_n_vars)

            resids['v'+str(self.ci)] = residuals

        else:
            residuals = np.empty(self.n_vars[self.ci - 1])
            for i in xrange(self.gt + 1, self.lt):
                Ai = self.A[counter]
                size = self.Aisizes[counter]
                res = 0
                for r in range(1, self.d_poly + 1): # for each degree

                    AA = np.ones(r, dtype=np.int) # array for term arguments
                    B = np.ones(r, dtype=np.int) # array for mapped term arguments
                    rfact = 1. / math.factorial(r)
                    fr = fr_func(r)

                    for ai1 in xrange(1, size**r + 1):

                        for k in range(r): # Mapping
                            B[k] = Ai[AA[k] - 1]

                        product = coefficient(rfact, fr, i, B, r)

                        for k in range(r): # one term of Taylor exp
                            Bk = B[k]
                            product *= (v[Bk - 1] - Bk)

                        res += product

                        for ai2 in range(r): # update arguments for term
                            if ai1%(size**ai2) == 0:
                                index = r - ai2 - 1
                                AA[index] += 1
                                if AA[index] > size:
                                    AA[index] = 1
                residuals[counter] = res
                counter += 1
            resids['v'+str(self.ci)] = residuals
        v = None # free memory

    def linearize(self, params, unknowns, resids):
        """Analytical derivatives."""
        self.par_deriv_count += 1
        print(self.ci, self.par_deriv_count, "par der"); sys.stdout.flush()
        v = np.empty(0)
        for i in xrange(1, self.n_comps + 1):
            if i == self.ci:
                v = np.append(v, unknowns['v'+str(i)])
            else:
                try:
                    v = np.append(v, params['v'+str(i)])
                except:
                    v = np.append(v, np.zeros(self.n_vars[i - 1]))

        Awidth = self.A.shape[1]
        counter = 0
        J = {}

        if fortranflag:

            pd = lib.returnpder(self.which_comps, self.which_comps.size, self.gt,
            self.lt, self.d_poly, self.A, Awidth, self.Aisizes, v, self.total_n_vars,
            self.cols_req_for_pd, self.empty_before, self.last_ind, self.n_comps)

            for i in xrange(self.which_comps.size):
                J[('v'+str(self.ci), 'v'+str(self.which_comps[i]))] = \
                pd[:, self.full_before[i]:self.full_before[i] + self.n_vars[self.which_comps[i]-1]]

        else:
            pd = np.zeros((self.n_vars[self.ci - 1], self.total_n_vars))
            for i in xrange(self.gt + 1, self.lt):
                Ai = self.A[counter]
                size = self.Aisizes[counter]
                for j in xrange(size):
                    p = Ai[j]
                    par_deriv = 0
                    for r in range(1, self.d_poly + 1): # for each degree

                        AA = np.ones(r, dtype=np.int) # array for term arguments
                        B = np.ones(r, dtype=np.int) # array for mapped term arguments
                        rfact = 1. / math.factorial(r)
                        fr = fr_func(r)

                        for ai1 in xrange(1, size**r + 1):

                            for k in range(r): # Mapping
                                B[k] = Ai[AA[k] - 1]

                            occurance = np.in1d(B, p).sum()

                            if occurance > 0:

                                product = coefficient(rfact, fr, i, B, r)
                                occ_switch = True
                                for k in range(r): # one term of Taylor exp
                                    Bk = B[k]
                                    if occ_switch and Bk == p:
                                        product *= occurance
                                        occ_switch = False
                                    else:
                                        product *= (v[Bk - 1] - Bk)

                                par_deriv += product

                            for ai2 in range(r): # update arguments for term
                                if ai1%(size**ai2) == 0:
                                    index = r - ai2 - 1
                                    AA[index] += 1
                                    if AA[index] > size:
                                        AA[index] = 1

                    for q in xrange(self.which_comps.size):
                        if p <= self.last_ind[self.which_comps[q] - 1]:
                            pd[counter, p - 1 - self.empty_before[q]] = par_deriv
                            break
                counter += 1

            for i in xrange(self.which_comps.size):
                J[('v'+str(self.ci), 'v'+str(self.which_comps[i]))] = \
                pd[:, self.full_before[i]:self.full_before[i] + self.n_vars[self.which_comps[i]-1]]

        v = None #free memory

        return J

# =============================================================================
# Class definition
# =============================================================================

class ScalableIneqConstComp(Component):
    """
    Scalable component used for inequality constraints.
    """

    def __init__(self, ci, n_vars, last_ind, d_poly, A, Aisizes, which_comps):
        super(ScalableIneqConstComp, self).__init__()

        self.ci = ci # component index
        self.n_vars = n_vars
        self.n_comps = n_vars.size # no. of components
        self.total_n_vars = n_vars.sum() # total no. of variables
        self.last_ind = last_ind
        self.gt = self.last_ind[ci - 2] # this component is for eqs with indices greater than this and
        self.lt = self.last_ind[ci - 1] + 1 # less than this

        if type(d_poly) is int:
            self.d_poly = d_poly
        else:
            self.d_poly = d_poly[ci - 1]

        self.A = A
        self.Aisizes = Aisizes
        self.which_comps = np.sort(which_comps)
        self.cols_req_for_pd = sum([n_vars[i - 1] for i in which_comps]) # used later for analytical partial deriv calcs

        if fortranflag:
            self.full_before, self.empty_before = lib.befores(self.n_vars,
                    self.n_comps, self.which_comps, self.which_comps.size)
            # used later for analytical partial deriv calcs
        else:
            self.empty_before = np.zeros(self.which_comps.size, dtype=int) # used later for analytical partial deriv calcs
            self.full_before = np.zeros(self.which_comps.size, dtype=int) # used later for analytical partial deriv calcs
    	    for i in xrange(self.which_comps.size):
    	        for j in xrange(self.which_comps[i] - 1):
                    if np.any(self.which_comps == j + 1):
                        self.full_before[i] += n_vars[j]
                    else:
    			        self.empty_before[i] += n_vars[j]

        self.add_output('C'+str(self.ci), val=np.zeros(n_vars[self.ci - 1]))

        for i in xrange(1, self.which_comps.size + 1):
            if self.which_comps[i - 1] == self.ci:
                print("Error: which_comps_list should not contain self index")
                exit()
            else:
                self.add_param('v'+str(self.which_comps[i - 1]), val=np.zeros(n_vars[self.which_comps[i - 1] - 1]))

        #self.deriv_options['type'] = 'fd'
        self.resids_count = 0 # for counting residual evaluation calls
        self.par_deriv_count = 0 # for counting par deriv evaluation calls

    def solve_nonlinear(self, params, unknowns, resids):

        self.resids_count += 1
        print(self.ci, self.resids_count, "resid"); sys.stdout.flush()
        v = np.empty(0)
        for i in xrange(1, self.n_comps + 1):
            if i == self.ci:
                v = np.append(v, np.zeros(self.n_vars[i - 1]))
            else:
                try:
                    v = np.append(v, params['v'+str(i)])
                except:
                    v = np.append(v, np.zeros(self.n_vars[i - 1]))
        # print(v); print(self.total_n_vars); exit()

        Awidth = self.A.shape[1]

        counter = 0
        if fortranflag:
            residuals = lib.returnres(self.gt, self.lt, self.d_poly, self.A,\
                 Awidth, self.Aisizes, v, self.total_n_vars)

            unknowns['C'+str(self.ci)] = residuals

        else:
            residuals = np.empty(self.n_vars[self.ci - 1])
            for i in xrange(self.gt + 1, self.lt):
                Ai = self.A[counter]
                size = self.Aisizes[counter]
                res = 0
                for r in range(1, self.d_poly + 1): # for each degree

                    AA = np.ones(r, dtype=np.int) # array for term arguments
                    B = np.ones(r, dtype=np.int) # array for mapped term arguments
                    rfact = 1. / math.factorial(r)
                    fr = fr_func(r)

                    for ai1 in xrange(1, size**r + 1):

                        for k in range(r): # Mapping
                            B[k] = Ai[AA[k] - 1]

                        product = coefficient(rfact, fr, i, B, r)

                        for k in range(r): # one term of Taylor exp
                            Bk = B[k]
                            product *= (v[Bk - 1] - Bk)

                        res += product

                        for ai2 in range(r): # update arguments for term
                            if ai1%(size**ai2) == 0:
                                index = r - ai2 - 1
                                AA[index] += 1
                                if AA[index] > size:
                                    AA[index] = 1
                residuals[counter] = res
                counter += 1
            unknowns['C'+str(self.ci)] = residuals
        v = None # free memory

    def linearize(self, params, unknowns, resids):
        """Analytical derivatives."""
        self.par_deriv_count += 1
        print(self.ci, self.par_deriv_count, "par der"); sys.stdout.flush()
        v = np.empty(0)
        for i in xrange(1, self.n_comps + 1):
            if i == self.ci:
                v = np.append(v, np.zeros(self.n_vars[i - 1]))
            else:
                try:
                    v = np.append(v, params['v'+str(i)])
                except:
                    v = np.append(v, np.zeros(self.n_vars[i - 1]))

        Awidth = self.A.shape[1]
        counter = 0
        J = {}
        no_of_ineq_const = self.n_vars[self.ci - 1]
        if fortranflag:

            pd = lib.returnpder(self.which_comps, self.which_comps.size, self.gt,
            self.lt, self.d_poly, self.A, Awidth, self.Aisizes, v, self.total_n_vars,
            self.cols_req_for_pd, self.empty_before, self.last_ind, self.n_comps)

            for i in xrange(self.which_comps.size):
                J[('C'+str(self.ci), 'v'+str(self.which_comps[i]))] = \
                pd[:, self.full_before[i]:self.full_before[i] + self.n_vars[self.which_comps[i]-1]]

        else:
            pd = np.zeros((self.n_vars[self.ci - 1], self.total_n_vars))
            for i in xrange(self.gt + 1, self.lt):
                Ai = self.A[counter]
                size = self.Aisizes[counter]
                for j in xrange(size):
                    p = Ai[j]
                    par_deriv = 0
                    for r in range(1, self.d_poly + 1): # for each degree

                        AA = np.ones(r, dtype=np.int) # array for term arguments
                        B = np.ones(r, dtype=np.int) # array for mapped term arguments
                        rfact = 1. / math.factorial(r)
                        fr = fr_func(r)

                        for ai1 in xrange(1, size**r + 1):

                            for k in range(r): # Mapping
                                B[k] = Ai[AA[k] - 1]

                            occurance = np.in1d(B, p).sum()

                            if occurance > 0:

                                product = coefficient(rfact, fr, i, B, r)
                                occ_switch = True
                                for k in range(r): # one term of Taylor exp
                                    Bk = B[k]
                                    if occ_switch and Bk == p:
                                        product *= occurance
                                        occ_switch = False
                                    else:
                                        product *= (v[Bk - 1] - Bk)

                                par_deriv += product

                            for ai2 in range(r): # update arguments for term
                                if ai1%(size**ai2) == 0:
                                    index = r - ai2 - 1
                                    AA[index] += 1
                                    if AA[index] > size:
                                        AA[index] = 1

                    for q in xrange(self.which_comps.size):
                        if p <= self.last_ind[self.which_comps[q] - 1]:
                            pd[counter, p - 1 - self.empty_before[q]] = par_deriv
                            break
                counter += 1

            for i in xrange(self.which_comps.size):
               J[('C'+str(self.ci), 'v'+str(self.which_comps[i]))] = \
               pd[:, self.full_before[i]:self.full_before[i] + self.n_vars[self.which_comps[i]-1]]
        v = None #free memory

        return J


# =============================================================================
# Objective components
# =============================================================================

class QuadObjectiveComp(Component):
    """
    Objective component.
    """

    def __init__(self, ci, n_vars, last_ind, d_poly, A, Aisizes, which_comps):
        super(QuadObjectiveComp, self).__init__()

        self.ci = ci # component index
        self.n_vars = n_vars
        self.n_comps = n_vars.size # no. of components
        self.total_n_vars = n_vars.sum() # total no. of variables
        self.last_ind = last_ind
        self.gt = self.last_ind[ci - 2] # this component is for eqs with indices greater than this and
        self.lt = self.last_ind[ci - 1] + 1 # less than this

        if type(d_poly) is int:
            self.d_poly = d_poly
        else:
            self.d_poly = d_poly[ci - 1]

        self.A = A
        self.Aisizes = Aisizes
        self.which_comps = which_comps
        #self.deriv_options['type'] = 'fd'
        for i in xrange(1, self.which_comps.size + 1):
            if self.which_comps[i - 1] == ci:
                self.add_output('v'+str(ci), val=np.zeros(n_vars[ci - 1]))
            else:
                self.add_param('v'+str(which_comps[i - 1]), val=np.zeros(n_vars[self.which_comps[i - 1] - 1]))

    def solve_nonlinear(self, params, unknowns, resids):

        objective = 0.
        for index in xrange(self.which_comps.size):
            if self.which_comps[index] == self.ci:
                continue
            else:
                objective += np.dot(params['v'+str(self.which_comps[index])],
                                    params['v'+str(self.which_comps[index])])
        unknowns['v'+str(self.ci)] = np.array([objective])


    def linearize(self, params, unknowns, resids):
        """Analytical derivatives."""

        J = {}
        for index in xrange(self.which_comps.size):
            if self.which_comps[index] == self.ci:
                continue
            else:
                J['v'+str(self.ci), 'v'+str(self.which_comps[index])] = \
                2*params['v'+str(self.which_comps[index])][np.newaxis]

        return J


class LinObjectiveComp(Component):
    """
    Objective component.
    """

    def __init__(self, ci, n_vars, last_ind, d_poly, A, Aisizes, which_comps):
        super(LinObjectiveComp, self).__init__()

        self.ci = ci # component index
        self.n_vars = n_vars
        self.n_comps = n_vars.size # no. of components
        self.total_n_vars = n_vars.sum() # total no. of variables
        self.last_ind = last_ind
        self.gt = self.last_ind[ci - 2] # this component is for eqs with indices greater than this and
        self.lt = self.last_ind[ci - 1] + 1 # less than this

        if type(d_poly) is int:
            self.d_poly = d_poly
        else:
            self.d_poly = d_poly[ci - 1]

        self.A = A
        self.Aisizes = Aisizes
        self.which_comps = which_comps
        #self.deriv_options['type'] = 'fd'
        for i in xrange(1, self.which_comps.size + 1):
            if self.which_comps[i - 1] == ci:
                self.add_output('v'+str(ci), val=np.zeros(n_vars[ci - 1]))
            else:
                self.add_param('v'+str(which_comps[i - 1]), val=np.zeros(n_vars[self.which_comps[i - 1] - 1]))

    def solve_nonlinear(self, params, unknowns, resids):

        objective = 0.
        for index in xrange(self.which_comps.size):
            if self.which_comps[index] == self.ci:
                continue
            else:
                objective += np.sum(params['v'+str(self.which_comps[index])])
        unknowns['v'+str(self.ci)] = np.array([objective])


    def linearize(self, params, unknowns, resids):
        """Analytical derivatives."""

        J = {}
        for index in xrange(self.which_comps.size):
            if self.which_comps[index] == self.ci:
                continue
            else:
                J['v'+str(self.ci), 'v'+str(self.which_comps[index])] = \
                np.ones(params['v'+str(self.which_comps[index])].size)[np.newaxis]

        return J


# =============================================================================
# Grouping
# =============================================================================

class DisGroup(Group):

    def __init__(self, n_vars, last_ind, d_poly, Alist, Aisizelist, comp_range,
                 which_comps_list, n_ind_var_comps, n_ineq_const_comps, d_obj):
        super(DisGroup, self).__init__()

        for ci in xrange(comp_range[0], comp_range[1] + 1):
            if ci <= n_ind_var_comps: # for design variables
                self.add('v_des'+str(ci), IndepVarComp('v'+str(ci), np.ones(n_vars[ci - 1])), promotes=['*'])
            elif (ci >= n_vars.size - n_ineq_const_comps) and ci <= n_vars.size - 1:
                self.add('dis'+str(ci), ScalableIneqConstComp(ci, n_vars, last_ind, d_poly,
                Alist[ci - 1], Aisizelist[ci - 1], which_comps_list[ci - 1]), promotes=['*'])
            elif ci==n_vars.size: # for objective function
                if d_obj == 1:
                    self.add('obj_comp', LinObjectiveComp(ci, n_vars, last_ind, d_poly,
                    Alist[ci - 1], Aisizelist[ci - 1], which_comps_list[ci - 1]), promotes=['*'])
                elif d_obj == 2:
                    self.add('obj_comp', QuadObjectiveComp(ci, n_vars, last_ind, d_poly,
                    Alist[ci - 1], Aisizelist[ci - 1], which_comps_list[ci - 1]), promotes=['*'])
                else:
                    print("d_obj can only be 1 or 2 for now"); exit()
            else:
                self.add('dis'+str(ci), ScalableComp(ci, n_vars, last_ind, d_poly,
                Alist[ci - 1], Aisizelist[ci - 1], which_comps_list[ci - 1]), promotes=['*'])


        self.nl_solver = Newton()
        self.nl_solver.options['rtol'] = 1.0e-12
        # self.nl_solver.options['alpha'] = 1.
        self.nl_solver.options['maxiter'] = 1000
        #self.ln_solver = ScipyGMRES()
        self.ln_solver = DirectSolver()

class ProbGroup(Group):

    def __init__(self):
        super(ProbGroup, self).__init__()

        self.nl_solver = Newton()
        self.nl_solver.options['rtol'] = 1.0e-10
        self.nl_solver.options['alpha'] = 1.
        self.nl_solver.options['maxiter'] = 1000
        #self.ln_solver = ScipyGMRES()
        self.ln_solver = DirectSolver()

# =============================================================================
# Automated group instantiation for hierarchical input
# =============================================================================

def auto_group_inst(dhl, n_vars, last_ind, d_poly, Alist, Aisizelist):
    """ Instantiates groups as described using add_hier_info. """

    add_hier_dict = dhl[0]
    add_range_dict = dhl[1]
    which_comps_list = dhl[2]

    Grouplist = []

    for loop in xrange(1, len(add_hier_dict) + 1):
        if type(eval('add_hier_dict["G"+str(%d)]' %loop)[0]) == int:
            print("Added component group G", loop); sys.stdout.flush()
            g_temp = DisGroup(n_vars, last_ind, d_poly, Alist, Aisizelist,
                              eval('add_hier_dict["G"+str(%d)]' %loop), which_comps_list)
            exec('G%d = g_temp' %loop)
            Grouplist.append(g_temp)

        elif type(eval('add_hier_dict["G"+str(%d)]' %loop)[0]) == str:
            print("Added group group G", loop); sys.stdout.flush()
            temp_list = eval('add_hier_dict["G"+str(%d)]' %loop)
            g_temp = ProbGroup()
            exec('G%d = g_temp' %loop)
            for loop2 in xrange(len(temp_list)):
                g_temp2 = eval(temp_list[loop2])
                g_temp.add(temp_list[loop2], g_temp2, promotes=['*'])
            Grouplist.append(g_temp)

    return Grouplist

# =============================================================================
# =============================================================================
# Input and Execution
# =============================================================================
# =============================================================================

# for now, the design var components have to come first, then the analysis
# components, then the inequality constraint components, and finally the
# objective component.

n_vars = np.array([10,15,10,15,10,15,10,15,15,10,10,1]) # np.array([5,5,5,5,5,5,5,5,5,5,5,1])
d_poly = 1#np.array([1,1,1,1,1,1,1,1,1,1,1,1])
comp_struct = [1,1,3,3,3,3,3,3,3,"C","C",1]
off_diag_comp_struct = [2.51, 2.1, 2.51, 2.1, 2.3, 2.3, 2.51, 2.1, 2.1, 2.1, 2.1, 2.7]
n_ind_var_comps = 2
n_ineq_const_comps = 2
d_obj = 2

# THREE OPTIONS FOR DESCRIBING STRUCTURE. SELECT ONE.
# 1. PROVIDE DSM_T REPRESENTING ENTIRE PROBLEM.
providing_DSM = True
# 2. DESCRIBE STRUCTURE AT DIFFERENT LEVELS USING add_hier_info
describing_hierarch = False
# 3. DIRECTLY PROVIDE CUSTOM which_comps_list LIST OF ARRAYS.
direct_which_comps = False

# ==========================================================================
# OPTION 1: Provide DSM_T representing entire problem
# ==========================================================================
assert (providing_DSM+describing_hierarch+direct_which_comps == 1)
if providing_DSM: # provide DSM here
    DSM_T = np.array(([1,0,0,0,0,0,0,0,0,0,0,0],
                      [0,1,0,0,0,0,0,0,0,0,0,0],
                      [1,1,1,1,1,0,0,1,0,0,0,0],
                      [1,0,1,1,1,0,0,1,1,0,0,0],
                      [1,0,0,1,1,1,1,0,1,0,0,0],
                      [1,0,1,0,1,1,0,0,0,0,0,0],
                      [1,0,0,0,1,0,1,1,1,0,0,0],
                      [1,0,1,1,0,0,1,1,0,0,0,0],
                      [1,0,0,0,1,0,0,1,1,0,0,0],
                      [0,0,0,0,1,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,1,0,0,0,0],
                      [1,1,1,0,1,1,0,1,1,1,0,1]))

    #DSM_T = DSM_T.T5mory efficient list
    which_comps_list = create_which_comps_list_from_DSM_T(DSM_T)

# ==========================================================================
# OPTION 2: Describe the problem using a hierarchical structure
# ==========================================================================
elif describing_hierarch:

    add_hier_dict = {}
    add_range_dict = {}
    which_comps_list = []
    for i in xrange(n_vars.size):
        which_comps_list.append(np.empty(0, dtype = np.int))
    dhl = [add_hier_dict, add_range_dict, which_comps_list]

    # Groups with components
    add_hier_info(dhl, "G1", "CG", [1])

    add_hier_info(dhl, "G2", "CG", [2,4], "UH")

    add_hier_info(dhl, "G3", "CG", [5,7], np.array(([1,0,0],
                                                    [1,1,0],
                                                    [1,1,1])))
    add_hier_info(dhl, "G4", "CG", [8,10], np.array(([1,0,0],
                                                     [1,1,0],
                                                     [1,1,1])))
    add_hier_info(dhl, "G5", "CG", [11], np.array(([1])))

    #Groups with groups
    add_hier_info(dhl, "G6", "GG", ["G2", "G3", "G4"], "UH")

    add_hier_info(dhl, "G7", "GG", ["G1", "G6", "G5"], np.array(([1   ,0   ,0],
                                                                 [1.99,1   ,0],
                                                                 [1.99,1.99,1])))

# ==========================================================================
# OPTION 3: Directly provide custom which_comps_list here
# ==========================================================================

elif direct_which_comps: # create which_comps_list here

    which_comps_list = []
    for i in range(1, n_vars.size + 1):
        which_comps_list.append(np.arange(i - 1, i + 2))
    which_comps_list[0] = np.array([1,2])
    which_comps_list[n_vars.size - 1] = np.array([n_vars.size - 1, n_vars.size ])

# ==========================================================================
# Call pre-processing step
# ==========================================================================
np.save("which_comps", which_comps_list)
try:
    which_comps_list = np.load("new_which_comps.npy")
except:
    pass
#plot_comp_jaco() # Preview inter-component Jacobian structure

last_ind = getlastind(n_vars)
Alist, Aisizelist = run_pre_proc(n_vars, last_ind, d_poly, comp_struct, which_comps_list, off_diag_comp_struct)
print(Alist)
# ==========================================================================
# Automated grouping for OPTION 2
# ==========================================================================

# Grouplist = auto_group_inst(dhl, n_vars, last_ind, d_poly, Alist, Aisizelist)
#
# for i in xrange(1, len(add_hier_dict) + 1):
#     exec('G%d = Grouplist[%d-1]' %(i,i))
#
# add_hier_info = None #free memory
# add_hier_dict = None #free memory
#
# top = Problem()
# last_group = len(add_hier_dict)
# exec("top.root = G%d" %last_group)
# top.driver = ScipyOptimizer()
# top.driver.options['optimizer'] = 'SLSQP'
# top.driver.options['tol'] = 1.0e-6
# top.driver.add_desvar('v1', lower=-10, upper=20)
# top.driver.add_objective('v'+str(n_vars.size))
# top.setup()

# ==========================================================================
# Single group
# ==========================================================================

top = Problem()
top.root = DisGroup(n_vars, last_ind, d_poly, Alist, Aisizelist,
                    [1, n_vars.size], which_comps_list, n_ind_var_comps,
                    n_ineq_const_comps, d_obj)
# top.driver = pyOptSparseDriver()
# top.driver.options['optimizer'] = 'SNOPT'
top.driver = ScipyOptimizer()
top.driver.options['optimizer'] = 'SLSQP'
top.driver.options['tol'] = 1.0e-4
top.driver.add_desvar('v1', lower=-100, upper=200)
top.driver.add_desvar('v2', lower=-100, upper=200)
top.driver.add_constraint('C'+str(n_vars.size - 1), upper = 50.0)
top.driver.add_constraint('C'+str(n_vars.size - 2), upper = 50.0)
top.driver.add_objective('v'+str(n_vars.size))

recorder = SqliteRecorder('OptRecorder')
top.driver.add_recorder(recorder)
top.setup()
#top.setup(check=False)

# ==========================================================================
# Manual grouping for multiple groups
# ==========================================================================

# top = Problem()
# G1 = DisGroup(n_vars, last_ind, d_poly, Alist, Aisizelist, [1,2], which_comps_list, n_ind_var_comps)
# G2 = DisGroup(n_vars, last_ind, d_poly, Alist, Aisizelist, [3,4], which_comps_list, n_ind_var_comps)
# G3 = DisGroup(n_vars, last_ind, d_poly, Alist, Aisizelist, [5,6], which_comps_list, n_ind_var_comps)
# G4 = DisGroup(n_vars, last_ind, d_poly, Alist, Aisizelist, [7,8], which_comps_list, n_ind_var_comps)
# G5 = DisGroup(n_vars, last_ind, d_poly, Alist, Aisizelist, [9,11], which_comps_list, n_ind_var_comps)
#
# top.root = ProbGroup()
# top.root.add('g1', G1, promotes=['*'])
# top.root.add('g2', G2, promotes=['*'])
# top.root.add('g3', G3, promotes=['*'])
# top.root.add('g4', G4, promotes=['*'])
# top.root.add('g5', G5, promotes=['*'])
#
# top.setup()

# ==========================================================================
# Initial guesses and run command
# ==========================================================================
for i in xrange(1, n_vars.size-1):
    top['v'+str(i)] = 100 * np.ones(n_vars[i-1])

top.run()
#data = top.check_partial_derivatives(out_stream=sys.stdout)

# ==========================================================================
# Print solutions and residuals
# ==========================================================================

print("Solutions:\n")
for i in xrange(1, n_vars.size - n_ineq_const_comps):
    print('v'+str(i), ' = ', top['v'+str(i)])
print("\nResiduals:\n")
for i in xrange(1, n_vars.size - n_ineq_const_comps):
    print('Resids v'+str(i), ' = ', top.root.resids['v'+str(i)])
print('Objective is', top['v'+str(n_vars.size)])
# print("\nNo. of times resids are evaluated:\n")
# for i in xrange(1, n_vars.size + 1):
#     print("component",i, eval("top.root.dis%d.resids_count" %i))
# print("\nNo. of times partial derivs are evaluated:\n")
# for i in xrange(1, n_vars.size + 1):
#     print("component",i, eval("top.root.dis%d.par_deriv_count" %i))
