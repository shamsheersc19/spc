#/bin/sh

SRC_FILES = calc_res.f90

default:
	f2py -c --fcompiler=gnu95 --f90flags="-fdefault-real-8 -fdefault-double-8" ${SRC_FILES} -m lib
