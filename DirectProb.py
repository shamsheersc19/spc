# =============================================================================
# External Python modules
# =============================================================================
from __future__ import print_function
import math
import numpy as np
from openmdao.api import Component, Group, NLGaussSeidel, ScipyGMRES, Problem, \
Newton, DirectSolver

# ==========================================================================
# Nonlinearity and coupling functions
# ==========================================================================

def fr_func(r):
    """ User defined nonlinearity function."""
    return 1.5**(-r)

def g_func(i, Ak):
    """ User defined coupling function."""
    return 1.5**(-abs(i - Ak))

# ==========================================================================
# Function for calculating the coefficients of a terms in Taylor series
# ==========================================================================

def coefficient(rfact, fr, i, B, r):
    """ Function for calculating coefficients of Taylor terms."""

    partial_deriv = fr * g_func(i, B[0])

    for k in range(1, r): # calculate coeff of term
        partial_deriv *= g_func(i, B[k])

    return rfact*partial_deriv

# =============================================================================
# Function for list of term indices and coefficients
# =============================================================================
def GenerateProblem(ci, n_vars, d_poly, structure):
    """
    Returns two lists. The first is a list of indices for the permutations in
    which the variables appear in each term of the Taylor expansions. The second
    contains the corresponding coeffecients for each term.

    Parameters
    ----------

    n_vars : numpy array
        An array with integers. The number of elements in the array corresponds
        to the number of components. The value of each element corresponds to
        the number of equations for that component. The sum of the elements is
        the total number of variables.

    d_poly : numpy array or int
        An array of integers or a single integer corresponding to the degree of
        the polynomials. If an array is provided then each integer corresponds
        to the degree of each component. If a single integer is provided then
        all the polymonials are based on this degree.

    structure : str or odd int or list
        This controls the Jacobian structure.
        'LT' for lower triangular, 'UT' for upper triangular,
        'LH' for lower Hessenberg, 'UH for upper Hessenberg',
        and an odd integer n for n-diagonal.
        If a list is provided, then each element corresponds to the structure of
        each corresponding component.
        If a single string or integer is provided, it is the structure for the
        entire system.

    """

    n_comps = n_vars.size # no. of components
    total_n_vars = n_vars.sum() # total no. of variables

    indices = np.zeros((n_comps + 1),dtype=np.int)
    for i in xrange(n_comps):
        indices[i] = np.sum(n_vars[0 : i + 1])
        # indices contains an array based on n_vars where the preceding elements
        # of each element is added to the element. Also, a 0 is appended to the
        # array. e.g. if n_vars is [5,10,15] then indices is [5,15,30,0].
        # This is used to determine the incides of the eqs in each component.

    gt = indices[ci - 2] # this component is for eqs with indices greater than this and
    lt = indices[ci - 1] + 1 # less than this

    if type(d_poly) is int:
        d_poly = d_poly
    else:
        d_poly = d_poly[ci - 1]

    if type(structure) is list:
        struct = structure[ci - 1]
    else:
        struct = structure

    indList = []
    coeffList = []

    for i in xrange(gt + 1, lt): # loop over no. of eqs in current component

        for r in range(1, d_poly + 1): # for each degree
            A = np.ones(r, dtype=np.int) # array for term arguments
            rfact = 1. / math.factorial(r)
            fr = fr_func(r)

            if type(struct) is int: # i.e. n-diagonal
                shift = (struct +1)/2 - i
                for ai1 in xrange(1, struct**r + 1):
                    B = A.copy() - np.ones(r, dtype=np.int)*shift
                    coeff = coefficient(rfact, fr, i, B, r)
                    if (np.min(B) < 1) or (np.max(B) > total_n_vars):
                        B = np.zeros(1)

                    coeffList.append(coeff)
                    indList.append(B.copy())

                    for ai2 in range(r): # update arguments for term
                        if ai1%(struct**ai2) == 0:
                            index = r - ai2 - 1
                            A[index] += 1
                            if A[index] > struct:
                                A[index] = 1

            elif struct == "UT": # i.e. upper-tri
                shift = (i - 1)
                n = total_n_vars - i + 1
                for ai1 in xrange(1, n**r + 1):
                    B = A + np.ones(r, dtype=np.int)*shift
                    coeff = coefficient(rfact, fr, i, B, r)
                    if (np.min(B) < 1) or (np.max(B) > total_n_vars):
                        B = np.zeros(1)

                    coeffList.append(coeff)
                    indList.append(B.copy())

                    for ai2 in range(r): # update arguments for term
                        if ai1%(n**ai2) == 0:
                            index = r - ai2 - 1
                            A[index] += 1
                            if A[index] > n:
                                A[index] = 1

            elif struct == "LT": # i.e. lower-tri

                for ai1 in xrange(1, i**r + 1):
                    B = A.copy()
                    coeff = coefficient(rfact, fr, i, B, r)
                    if (np.min(B) < 1) or (np.max(B) > total_n_vars):
                        B = np.zeros(1)

                    coeffList.append(coeff)
                    indList.append(B.copy())

                    for ai2 in range(r): # update arguments for term
                        if ai1%(i**ai2) == 0:
                            index = r - ai2 - 1
                            A[index] += 1
                            if A[index] > i:
                                A[index] = 1

            elif struct == "UH": # i.e. upper-Hessenberg
                shift = (i - 2)
                n = total_n_vars - i + 2
                for ai1 in xrange(1, n**r + 1):
                    B = A + np.ones(r, dtype=np.int)*shift
                    coeff = coefficient(rfact, fr, i, B, r)
                    if (np.min(B) < 1) or (np.max(B) > total_n_vars):
                        B = np.zeros(1)

                    coeffList.append(coeff)
                    indList.append(B.copy())

                    for ai2 in range(r): # update arguments for term
                        if ai1%(n**ai2) == 0:
                            index = r - ai2 - 1
                            A[index] += 1
                            if A[index] > n:
                                A[index] = 1

            elif struct == "LH": # i.e. lower-Hessenberg
                n = i+1
                for ai1 in xrange(1, n**r + 1):

                    B = A.copy()
                    coeff = coefficient(rfact, fr, i, B, r)
                    if (np.min(B) < 1) or (np.max(B) > total_n_vars):
                        B = np.zeros(1)

                    coeffList.append(coeff)
                    indList.append(B.copy())

                    for ai2 in range(r): # update arguments for term
                        if ai1%(n**ai2) == 0:
                            index = r - ai2 - 1
                            A[index] += 1
                            if A[index] > n:
                                A[index] = 1

            else:

                for ai1 in xrange(1, total_n_vars**r + 1):

                    B = A.copy()
                    coeff = coefficient(rfact, fr, i, B, r)
                    if (np.min(B) < 1) or (np.max(B) > total_n_vars ):
                        B = np.zeros(1)

                    coeffList.append(coeff)
                    indList.append(B.copy())

                    for ai2 in range(r): # update arguments for term
                        if ai1%(total_n_vars**ai2) == 0:
                            index = r - ai2 - 1
                            A[index] += 1
                            if A[index] > total_n_vars:
                                A[index] = 1

    return indList, coeffList

# =============================================================================
# Class definition
# =============================================================================
class ScalableComp(Component):
    """
    Scalable component.
    """

    def __init__(self, ci, n_vars, d_poly, structure, indlist, coefflist):
        super(ScalableComp, self).__init__()

        self.n_comps = n_vars.size # no. of components
        self.total_n_vars = n_vars.sum() # total no. of variables

        self.indices = np.zeros((self.n_comps + 1),dtype=np.int)
        for i in xrange(self.n_comps):
            self.indices[i] = np.sum(n_vars[0 : i + 1])
        # indices contains an array based on n_vars where the preceding elements
        # of each element is added to the element. Also, a 0 is added to the end
        # of the array. e.g. if n_vars is [5,10,15] then indices is [5,15,30,0].
        # This is used to determine the incides of the eqs in each component.

        self.gt = self.indices[ci - 2] # this component is for eqs with indices greater than this and
        self.lt = self.indices[ci - 1] + 1 # less than this

        if type(d_poly) is int:
            self.d_poly = d_poly
        else:
            self.d_poly = d_poly[ci - 1]

        if type(structure) is list:
            self.struct = structure[ci - 1]
        else:
            self.struct = structure

        self.indlist = indlist
        self.coefflist = coefflist

        for i in xrange(1, self.gt + 1):
            self.add_param('v'+str(i), val=0.)
        for i in xrange(self.gt + 1, self.lt):
            self.add_state('v'+str(i), val=0.)
        for i in xrange(self.lt, self.total_n_vars + 1 ):
            self.add_param('v'+str(i), val=0.)

        self.fd_options['force_fd'] = True

    def solve_nonlinear(self, params, unknowns, resids):
        self.apply_nonlinear(params, unknowns, resids)

    def add_term(self, counter, A, v):

        if A[0] == 0:
            return 0.

        product = self.coefflist[counter]

        for k in range(A.size): # one term of Taylor exp
            Ak = A[k]
            product *= (v[Ak] - Ak)

        return product

    def apply_nonlinear(self, params, unknowns, resids):

        v = np.zeros(self.total_n_vars + 1)

        for i in xrange(1, self.gt + 1):
            v[i] = params['v'+str(i)]
        for i in xrange(self.gt + 1, self.lt):
            v[i] = unknowns['v'+str(i)]
        for i in xrange(self.lt, self.total_n_vars + 1):
            v[i] = params['v'+str(i)]

        counter = 0

        for i in xrange(self.gt + 1, self.lt): # loop over no. of eqs in current component

            var_name = 'v'+str(i)
            resids[var_name] = 0.

            if type(self.struct) is int: # i.e. n-diagonal
                n = self.struct

            elif self.struct == "UT": # i.e. upper-tri
                n = self.total_n_vars - i + 1

            elif self.struct == "LT": # i.e. lower-tri
                n = i

            elif self.struct == "UH": # i.e. upper-Hessenberg
                n = self.total_n_vars - i + 2

            elif self.struct == "LH": # i.e. lower-Hessenberg
                n = i+1

            else:
                n = self.total_n_vars

            for r in range(1, self.d_poly + 1): # for each degree

                ai1range = n**r

                for ai1 in xrange(ai1range):
                    A = self.indlist[counter]
                    resids[var_name] += self.add_term(counter, A, v)
                    counter+=1

# =============================================================================
# Group
# =============================================================================

class DisGroup(Group):

    def __init__(self, n_vars, d_poly, structure, indlist, coefflist):
        super(DisGroup, self).__init__()

        for ci in xrange(1, n_vars.size + 1):
            self.add('d'+str(ci), ScalableComp(ci, n_vars, d_poly, structure, indlist[ci - 1], coefflist[ci - 1]), promotes=['*'])

        self.nl_solver = Newton()
        self.nl_solver.options['atol'] = 1.0e-12
        self.nl_solver.options['alpha'] = 1.
        #self.ln_solver = ScipyGMRES()
        self.ln_solver = DirectSolver()
        #self.deriv_options['type'] = 'fd'


# =============================================================================
# Input and Execution
# =============================================================================
if __name__ == "__main__":

    n_vars = np.array([25,25,25,25])
    d_poly = np.array([2,2,2,2])
    structure = [5,5,5,5]

    indlist = []
    coefflist = []
    for ci in xrange(1, n_vars.size + 1):
        ilist, clist = GenerateProblem(ci, n_vars, d_poly, structure)
        indlist.append(ilist)
        coefflist.append(clist)

    top = Problem()
    top.root = DisGroup(n_vars, d_poly, structure, indlist, coefflist)
    top.setup()

    for i in xrange(1, n_vars.sum()+1):
        top['v'+str(i)] = 123.

    top.run()

    print("Solutions and residuals:")

    for i in xrange(1, n_vars.sum()+1):
        print('v'+str(i), ' = ', top['v'+str(i)])
    for i in xrange(1, n_vars.sum()+1):
        print('Resids v'+str(i), ' = ', top.root.resids['v'+str(i)])
