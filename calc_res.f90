subroutine returnres(gt, lt, dpoly, A, widthofA, Aisizes, v, sizeofv, residuals)

  implicit none

  !f2py intent(in) gt, lt, dpoly, A, widthofA, Aisizes, v, sizeofv
  !f2py intent(out) residuals
  !f2py depend(gt) A, Aisizes, residuals
  !f2py depend(lt) A, Aisizes, residuals
  !f2py depend(widthofA) A
  !f2py depend(sizeofv) v

  ! Input
  integer, intent(in) :: gt, lt, dpoly, A(lt - gt - 1, widthofA), widthofA
  integer, intent(in) :: Aisizes(lt - gt - 1), sizeofv
  real*8, intent(in) :: v(sizeofv)

  ! Output
  real*8, intent(out) :: residuals(lt - gt - 1)

  ! Working
  integer :: i, r, ai1, k, ai2, Bk, AA(dpoly), index, size, counter, Ai(widthofA)
  real*8 :: rfact, fr, gik, product, res, soln, alpha
  
  alpha = 1.0

  counter = 1
  do i = gt + 1, lt - 1

    AA(:) = 1
    res = 0.
    size = Aisizes(counter)
    Ai = A(counter, :)

    do r = 1, dpoly

      rfact = 1.
      do k = 2, r
        rfact = rfact*k
      end do
      rfact = 1. / rfact

      fr = 1. / r ! user defined nonlinearity function

      do ai1 = 1, size**r

        product = 1.
        do k = 1, r
          Bk = Ai(AA(k))
          gik = 1.0001**(-ABS(i - Bk)) ! user defined coupling function
          if ((Bk .LE. gt) .OR. (Bk .GE. lt)) then
            gik = gik * alpha
          end if
          soln = 0. ! solution value (Bk for index)
          product = product * gik * (v(Bk) - soln)
        end do

        res = res + rfact * fr * product

        do ai2 = 0, r - 1
          if (MODULO(ai1,(size**ai2)) == 0) then
            index = r - ai2
            AA(index) = AA(index) + 1
            if (AA(index) > size) then
              AA(index) = 1
            end if
          end if
        end do

      end do
    end do
  residuals(counter) = res
  counter = counter + 1
  end do
end subroutine returnres

subroutine returnpder(which_comps, which_comps_size, gt, lt, dpoly, A, widthofA,&
&Aisizes, v, sizeofv, cols_req_for_pd, empty_before, last_ind, n_comps, partials)

  implicit none

  !f2py intent(in) which_comps, which_comps_size, gt, lt, dpoly, A, widthofA, Aisizes, v, sizeofv
  !f2py intent(in) cols_req_for_pd, empty_before, last_ind, n_comps
  !f2py intent(out) partials
  !f2py depend(which_comps_size) which_comps, empty_before
  !f2py depend(gt) A, Aisizes, partials
  !f2py depend(lt) A, Aisizes, partials
  !f2py depend(widthofA) A
  !f2py depend(sizeofv) v
  !f2py depend(cols_req_for_pd) partials
  !f2py depend(n_comps) last_ind

  ! Input
  integer, intent(in) :: which_comps(which_comps_size), which_comps_size, gt, lt, dpoly
  integer, intent(in) :: A(lt - gt - 1, widthofA), widthofA
  integer, intent(in) :: Aisizes(lt - gt - 1), sizeofv, cols_req_for_pd
  integer, intent(in) :: empty_before(which_comps_size), last_ind(n_comps + 1), n_comps
  real*8, intent(in) :: v(sizeofv)

  ! Output
  real*8, intent(out) :: partials(lt - gt - 1, cols_req_for_pd)

  ! Working
  integer :: i, j, r, ai1, k, ai2, Bk, AA(dpoly), index, size, counter, Ai(widthofA)
  integer :: occurance, p, q, condensed_last_ind(which_comps_size)
  logical :: occ_switch
  real*8 :: rfact, fr, gik, product, par_deriv, soln, alpha
  
  alpha = 1.0

  do i = 1, which_comps_size
    condensed_last_ind(i) = last_ind(which_comps(i))
  end do

  partials(:,:) = 0.

  counter = 1
  do i = gt + 1, lt - 1

    AA(:) = 1
    size = Aisizes(counter)
    Ai = A(counter, :)

    do j = 1, size
      p = Ai(j)
      par_deriv = 0.

      do r = 1, dpoly

        rfact = 1.
        do k = 2, r
          rfact = rfact*k
        end do
        rfact = 1. / rfact

        fr = 1. / r ! user defined nonlinearity function

        do ai1 = 1, size**r

          occurance = 0
          do k = 1, r
            if (Ai(AA(k)) == p) then
              occurance = occurance + 1
            end if
          end do

          if (occurance > 0) then
            product = 1.
            occ_switch = .True.
            do k = 1, r
              Bk = Ai(AA(k))
              if (occ_switch .and. (Bk == p)) then
                gik = 1.0001**(-ABS(i - Bk)) ! user defined coupling function
                if ((Bk .LE. gt) .OR. (Bk .GE. lt)) then
                  gik = gik * alpha
                end if
                product = product * gik * occurance
                occ_switch = .False.
              else
                gik = 1.0001**(-ABS(i - Bk)) ! user defined coupling function
                if ((Bk .LE. gt) .OR. (Bk .GE. lt)) then
                  gik = gik * alpha
                end if
                soln = 0. ! solution value (Bk for index)
                product = product * gik * (v(Bk) - soln)
              end if

            end do
            par_deriv = par_deriv + rfact * fr * product
          end if

          do ai2 = 0, r - 1
            if (MODULO(ai1,(size**ai2)) == 0) then
              index = r - ai2
              AA(index) = AA(index) + 1
              if (AA(index) > size) then
                AA(index) = 1
              end if
            end if
          end do
        end do
      end do

      do q = 1, which_comps_size
        if (p .LE. condensed_last_ind(q)) then
          partials(counter, p - empty_before(q)) = par_deriv
          exit
        end if
      end do

    end do
    counter = counter + 1
  end do

end subroutine returnpder

subroutine befores(n_vars, n_comps, which_comps, which_comps_size, full, empty)

  implicit none

  !f2py intent(in) n_vars, n_comps, which_comps, which_comps_size
  !f2py intent(out) full, empty
  !f2py depend(n_comps) n_vars
  !f2py depend(which_comps_size) which_comps, full, empty

  ! Input
  integer, intent(in) :: n_vars(n_comps), n_comps, which_comps(which_comps_size)
  integer, intent(in) :: which_comps_size

  ! Output
  integer, intent(out) :: full(which_comps_size), empty(which_comps_size)

  ! Working
  integer :: i, j

  full(:) = 0
  empty(:) = 0

  do i = 1, which_comps_size
    do j = 1, which_comps(i) - 1
      if (any(which_comps==j)) then
        full(i) = full(i) + n_vars(j)
      else
        empty(i) = empty(i) + n_vars(j)
      end if
    end do
  end do

end subroutine befores

subroutine returnlinmatvec(gt, lt, A, widthofA, Aisizes, v, sizeofv, coeffmatrix, constvector)

  implicit none

  !f2py intent(in) gt, lt, A, widthofA, Aisizes, v, sizeofv
  !f2py intent(out) coeffmatrix, constvector
  !f2py depend(gt) A, Aisizes, coeffmatrix, constvector
  !f2py depend(lt) A, Aisizes, coeffmatrix, constvector
  !f2py depend(widthofA) A
  !f2py depend(sizeofv) v

  ! Input
  integer, intent(in) :: gt, lt, A(lt - gt - 1, widthofA), widthofA
  integer, intent(in) :: Aisizes(lt - gt - 1), sizeofv
  real*8, intent(in) :: v(sizeofv)

  ! Output
  real*8, intent(out) :: coeffmatrix(lt - gt - 1, lt - gt -1), constvector(lt - gt -1)

  ! Working
  integer :: i, ai1, Bk, size, counter, Ai(widthofA)
  real*8 :: fr, gik, coeff, soln, alpha
  
  alpha = 1.0

  coeffmatrix(:,:) = 0.
  constvector(:) = 0.

  counter = 1
  do i = gt + 1, lt - 1

    size = Aisizes(counter)
    Ai = A(counter, :)

    fr = 1. ! user defined nonlinearity function

    do ai1 = 1, size

      Bk = Ai(ai1)
      gik = 1.0001**(-ABS(i - Bk)) ! user defined coupling function
      if ((Bk .LE. gt) .OR. (Bk .GE. lt)) then
        gik = gik * alpha
      end if
      soln = 0. ! solution value (Bk for index)
      coeff = fr * gik

      if ( (Bk .GT. gt) .AND. (Bk .LT. lt) ) then
        coeffmatrix(i - gt, Bk - gt) = coeff
        constvector(i - gt) = constvector(i - gt) + coeff * soln
      else
        constvector(i - gt) = constvector(i - gt) - coeff * (v(Bk) - soln)
      end if

    end do

  counter = counter + 1
  end do
end subroutine returnlinmatvec
