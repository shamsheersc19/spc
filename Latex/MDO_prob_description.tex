\documentclass{mdolab-article}

% The mdolab-article \LaTeX class loads a number of packages by default; see mdolab-article.cls

% Start \LaTeX preamble ========================================================

\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{subcaption}
% Define custom color
\colorlet{lblue}{blue!50!black}

% Get an hyperlinked PDF: all citations and create PDF table of contents (without obnoxious colors)
\usepackage[bookmarks=true,bookmarksnumbered=true,colorlinks=true,linkcolor=lblue,citecolor=lblue,urlcolor=lblue]{hyperref}

% PDF metadata: make sure you update this for each document
\hypersetup{
  pdfauthor={Shamsheer Chauhan}, % insert author here
  pdftitle={Scalable Code Update}, % insert title here
  pdfsubject={Scalable code}, % insert keywords here
}

% Bibtex stuff:
\usepackage{natbib} % For bibtex \citet, \citep
% use the natbib options below when using the AIAA style:
% \usepackage[numbers,sort]{natbib}
\usepackage{hypernat} % To get natbib to play nicely with hyperref
\usepackage{doi} % For getting hyperlinked DOI in the references

\usepackage{dcolumn}

\usepackage{bm} % Bold math

\usepackage{esdiff} % Easy differentiation for \p and \f shortcuts

% XDSM diagram styles
%\input{./0figures/diagram_styles}

% For listing code
\usepackage{listings}
\lstset{
         basicstyle=\footnotesize\ttfamily, % Standardschrift
         %numbers=left,                % Ort der Zeilennummern
         numberstyle=\tiny,          % Stil der Zeilennummern
         %stepnumber=2,               % Abstand zwischen den Zeilennummern
         numbersep=5pt,              % Abstand der Nummern zum Text
         tabsize=2,                  % Groesse von Tabs
         extendedchars=true,         %
         breaklines=true,            % Zeilen werden Umgebrochen
         keywordstyle=\color{blue}\bfseries,
         %stringstyle=\color{white}\ttfamily, % Farbe der String
         showspaces=false,           % Leerzeichen anzeigen ?
         showtabs=false,             % Tabs anzeigen ?
         xleftmargin=17pt,
         framexleftmargin=17pt,
         framexrightmargin=5pt,
         framexbottommargin=4pt,
         %backgroundcolor=\color{lightgray},
         showstringspaces=false,      % Leerzeichen in Strings anzeigen ?
		     language=Python
 }

% Use this to skip specifying the graphics folder in includegraphics
\graphicspath{{.}}


% Some custom commands
\newcommand{\eqnref}[1]{(\ref{#1})} % To make parentheses automatically when referring to equations
\newcommand{\f}{\frac}
\newcommand{\lf}{\left}
\newcommand{\rg}{\right}
%\newcommand{\bm}[1]{\bm{#1}}
\renewcommand{\Im}{\operatorname{Im}}
\if@symbold\else\def\d{\,\mathrm{d}}\fi % the proper total derivative symbol
\newcommand{\degree}{\ensuremath{^\circ\,}} % degree symbol

\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}


% Start document specific part ================================================

\title{Scalable MDO problem}

\author{Shamsheer S. Chauhan - 2 Aug 2016}

\begin{document}

\maketitle

\section{MDO problem formulation}
\medskip
\noindent The problem has $N$ variables comprising $N_{DV}$ design 
variables and $N_{SCV}$ state and coupling variables. There are 
$N_{SCV}$ residual equations representing the governing equations 
and $N_{IC}$ inequalities for constraints. \\

\noindent The MDO problem formulation is 

\begin{equation}
\label{scalable}
\begin{tabular}{l  l  l}
minimize & $C_{i}(v_{1},\ldots,v_{N})$ & for $i=0$\\
&  e.g., $\sum_{i=1}^{N} k_i v_i^2$ \ or \ $ \sum_{i=1}^{N} k_i v_i$ & where $k_i = 1$ \ or \ $0$\\
& \\
with respect to & $v_i$ & for $i = 1,\ldots,{N_{DV}}$ \\
& \\
with governing equations & $C_{i}(v_{1},\ldots,v_{N})= 0$ & for $i = {N_{DV}}+1,\ldots,N$ \\
& \\
subject to & $C_{i}(v_{1},\ldots,v_{N}) - K_i \geq 0$ & for $i = N+1,\ldots,N+N_{IC}$\\
\end{tabular}
\end{equation} 

\noindent where

\begin{equation}
C_{i}(v_{1},\ldots,v_{N}) =
\sum_{r=0}^{d_{i}}\frac{1}{r!}
\mathop{\mathop{\sum_{(j_{1},\ldots,j_{r})}^{}}_{1 \leq j_{k} \leq N}}_{j_{1},\ldots,j_{r} \in \mathcal{A}(i) }
\frac{\partial^{r}C_{i}}{\partial v_{j_{1}} \ldots \partial v_{j_{r}}}
\prod\limits_{k=1}^{r} (v_{j_{k}} - v^*_{j_{k}})
\label{eq:taylor1}
\end{equation}

\begin{table}[h]
\caption{Nomenclature and descriptions.}
\centering
\begin{tabular}{c l l}
\hline
Parameter & Description & Characteristic controlled \\ [0.25ex]
%heading
\hline
$N$ & Total number of variables ($N = N_{DV} + N_{SCV}$) & Problem size\\[0.5ex]
$N_{DV}$ & Total number of design variables & \\[0.5ex]
$N_{SCV}$ & Total number of state and coupling variables & \\[0.5ex]
$N_{IC}$ & Total number of inequality constraints & \\[0.5ex]
$v_{i}$ & Variables & \\[0.5ex]
$d_{i}$ & Degree of polynomial & Nonlinearity\\[0.5ex]
$k_i, K_i, v_{i}^{*}$ & User-specified constants. Currently $v_{i}^{*} = i$ (except & \\[0.5ex]
& for the objective function and variable bounds). & \\[0.5ex]
${\mathcal{A}}(i)$ & Arguments of the $i^{th}$ equation. & Jacobian structure and coupling\\
 & $\mathcal{A}:\{1,\ldots,N\} \rightarrow \mathcal{P}(\{1,\ldots,N\})$ & \\
& where $\mathcal{P}()$ is the power set. & \\ [0.5ex]
\hline
\end{tabular}
\label{table:nomenclature1}
\end{table}

\noindent The partial derivative terms in Equation~\eqnref{eq:taylor1} are specified by 
the user. For the governing equations and inequality constraints 
(except bounds for design variables), the following formula is used. 

\begin{equation}
\frac{\partial^{r}C_{i}}{\partial v_{j_{1}} \ldots \partial v_{j_{r}}} =
f(r) \prod\limits_{k=1}^{r}g(i,j_{k})
\label{eq:pderivform1}
\end{equation}

\noindent Where $f(r)$ is a user-specified function decaying with $r$ and
$g(i,j_{k})$ is a user-specified function decaying with $\left| i - j_{k} \right|$. Bounds for design variables can also be represented using 
the inequality form in Equation \ref{scalable} by selecting 
appropriate values for $d_i$, $({j_{1},\ldots,j_{r}})$, 
$v_{i}^{*}$, $K_i$, and 
the partial derivative terms. Similarly, the example objective functions can also 
be derived from the RHS of Equation \ref{eq:taylor1} by 
selecting appropriate values. 

\pagebreak

\section{A couple example problems}
\medskip
\subsection{Example 1}
\medskip
\noindent 25 design variables.\\
20 linear inequality constraints (excluding upper and lower bounds for 
design variables). \\
90 linear residual equations representing 7 disciplines.\\
Quadratic objective function (sum of squares). \\
Optimizer: SLSQP, tol. = 1.0e-4. \\

\noindent Design variables at the top; constraints and 
objective function at the bottom: 

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{exmdo1.pdf}
    \caption{Jacobian structure for example 1. }
    \label{results1}
  \end{center}
\end{figure}

\begin{lstlisting}

Optimization terminated successfully.    (Exit mode 0)
            Current function value: [ 475850.83760571]
            Iterations: 22
            Function evaluations: 44
            Gradient evaluations: 22

real	1m3.684s
user	1m38.381s
sys   5m56.376s

\end{lstlisting}
\pagebreak

\subsection{Example 2}
\medskip
\noindent 25 design variables.\\
20 linear inequality constraints (excluding upper and lower bounds for 
design variables). \\
900 linear residual equations representing 7 disciplines.\\
Quadratic objective function (sum of squares). \\
Optimizer: SLSQP, tol. = 1.0e-4. \\

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.95\textwidth]{exmdo2.pdf}
    \caption{Jacobian structure for example 2. }
    \label{results2}
  \end{center}
\end{figure}

\begin{lstlisting}

Optimization terminated successfully.    (Exit mode 0)
            Current function value: [  2.23059143e+08]
            Iterations: 28
            Function evaluations: 50
            Gradient evaluations: 28

real	33m31.348s
user	65m40.147s
sys   195m15.679s

\end{lstlisting}


\pagebreak

\section{Analysis problem formulation for reference}

\medskip
The problem is a system of $n$ residual equations, depending on $n$ variables ($v_{1},\ldots,v_{n}$),
 of the following form.  
\begin{equation}
  \begin{array}{rcl} C_{1}(v_{1},\ldots,v_{n})&=& 0 \\ &\vdots \\ C_{n}(v_{1},\ldots,v_{n})&=& 0 \end{array}
\label{eq:system}
\end{equation}


\noindent Note that the number of residual equations is equal to the number of variables. The LHS of the $i^{th}$ equation in this system is obtained using the following Taylor polynomial form. 

\begin{equation}
C_{i}(v_{1},\ldots,v_{n}) =
\sum_{r=0}^{d_{i}}\frac{1}{r!}
\mathop{\mathop{\sum_{(j_{1},\ldots,j_{r})}^{}}_{1 \leq j_{k} \leq n}}_{j_{1},\ldots,j_{r} \in \mathcal{A}(i) }
\frac{\partial^{r}C_{i}}{\partial v_{j_{1}} \ldots \partial v_{j_{r}}}
\prod\limits_{k=1}^{r} (v_{j_{k}} - v^*_{j_{k}})
\label{eq:taylor2}
\end{equation}

\noindent The partial derivative terms in Equation~\eqnref{eq:taylor2} are specified by 
the user. Currently, the following formula is used. 

\begin{equation}
\frac{\partial^{r}C_{i}}{\partial v_{j_{1}} \ldots \partial v_{j_{r}}} =
f(r) \prod\limits_{k=1}^{r}g(i,j_{k})
\label{eq:pderivform}
\end{equation}

\noindent See Table \ref{table:nomenclature} for descriptions of the nomenclature used and the problem characteristic that each controls. 

\begin{table}[h]
\caption{Nomenclature and description.}
\centering
\begin{tabular}{c l l}
\hline
Parameter & Description & Characteristic controlled \\ [0.25ex]
%heading
\hline
$n$ & Number of variables & Problem size\\[0.5ex]
$d_{i}$ & Degree of polynomial & Nonlinearity\\[0.5ex]
$v_{i}^{*}$ & Solution value & Solution (set to zero WLOG) \\[0.5ex]
${\mathcal{A}}(i)$ & Arguments of the $i^{th}$ equation. & Jacobian structure and coupling\\
 & $\mathcal{A}:\{1,\ldots,n\} \rightarrow \mathcal{P}(\{1,\ldots,n\})$ & \\
& where $\mathcal{P}()$ is the power set. & \\ [0.5ex]
$f(r)$ & A user-specified function decaying with $r$ & Nonlinearity \\ [0.5ex]
$g(i,j_{k})$ & A user-specified function decaying with $\left| i - j_{k} \right|$ & Coupling strength\\ [0.25ex]
\hline
\end{tabular}
\label{table:nomenclature}
\end{table}


\end{document}
