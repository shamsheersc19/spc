import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams['text.usetex'] = True
# matplotlib.rcParams['text.latex.unicode'] = True
size = np.array([100,1000,10000])

# CPU time

# time_1 = np.array([0.381+0.707, 2.323 + 0.935, 5*60 + 30.543 + 4.307])
# time_2 = np.array([1.134+3.544, 25.612+7.751, 91*60 + 20.069 + 60 + 27.993])
# time_3 = np.array([4.223+8.861, 60+6.425 + 11.110, 114*60 + 36.800 +60+ 48.929])
# time_4 = np.array([49.756 + 10.667, 10*60+21.559+11.899, 216*60+21.058+120+0.208])
# print time_1, time_2, time_3, time_4
# # Res evals
#
# r_calls_1 = np.array([2, 2, 2])
# r_calls_2 = np.array([15, 17, 20])
# r_calls_3 = np.array([21, 25, 24])
# r_calls_4 = np.array([25, 26, 26])
#
# # Total no. of residual equation evalutations
#
# r_total_eval_1 = np.array([2*100, 2*1000, 2*10000])
# r_total_eval_2 = np.array([15*100, 17*1000, 20*10000])
# r_total_eval_3 = np.array([21*100, 25*1000, 24*10000])
# r_total_eval_4 = np.array([25*100, 26*1000, 26*10000])
#
# # Partial derivative evals
#
# pd_calls_1 = np.array([1, 1, 1])
# pd_calls_2 = np.array([14, 16, 19])
# pd_calls_3 = np.array([20, 24, 23])
# pd_calls_4 = np.array([24, 25, 25])
#
# # Total no. of residual equation evalutations
#
# pd_total_eval_1 = np.array([1*100, 1*1000, 1*10000])
# pd_total_eval_2 = np.array([14*100, 16*1000, 19*10000])
# pd_total_eval_3 = np.array([20*100, 24*1000, 23*10000])
# pd_total_eval_4 = np.array([24*100, 25*1000, 25*10000])
#
# # plt.subplot(131)
# # plt.semilogx(size, r_calls_1, marker='s', markersize = 7, color = 'r', label='Residual, $d=1$')
# # plt.semilogx(size, r_calls_2, marker='*', markersize = 10, color = 'y', label='Residual, $d=2$')
# # plt.semilogx(size, r_calls_3, marker='o', markersize = 7, color = 'g', label='Residual, $d=3$')
# # plt.semilogx(size, r_calls_4, marker='x', markersize = 10, color = 'b', label='Residual, $d=4$')
# # plt.semilogx(size, pd_calls_1, marker='s', markersize = 7, color = 'r', ls = '--', label='Partial derivative, $d=1$' )
# # plt.semilogx(size, pd_calls_2, marker='*', markersize = 10, color = 'y', ls = '--', label='Partial derivative, $d=2$' )
# # plt.semilogx(size, pd_calls_3, marker='o', markersize = 7, color = 'g', ls = '--', label='Partial derivative, $d=3$' )
# # plt.semilogx(size, pd_calls_4, marker='x', markersize = 10, color = 'b', ls = '--', label='Partial derivative, $d=4$' )
# # plt.grid(True)
# # plt.xlabel('$n$', fontsize = 22)
# # plt.ylabel('No. of evaluations', fontsize = 18)
# # plt.gca().set_xlim([50,20000])
# # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize = 15)
# # plt.xticks(fontsize = 15)
# # plt.yticks(fontsize = 15)
# # plt.show()
#
#
# data_to_plot1 = [r_calls_1, r_calls_2, r_calls_3, r_calls_4]
# #data_to_plot2 = [pd_calls_1, pd_calls_2, pd_calls_3, pd_calls_4]
# plt.boxplot(data_to_plot1)
# #plt.boxplot(data_to_plot2)
# plt.xlabel('Degree of polynomials, $d$', fontsize = 18)
# plt.ylabel('No. of residual evaluations', fontsize = 18)
# plt.xticks(fontsize = 15)
# plt.yticks(fontsize = 15)
# plt.show()
#
#
# plt.subplot(121)
# plt.loglog(size, time_1, marker='s', markersize = 7, color = 'r', label='$d=1$')
# plt.loglog(size, time_2, marker='*', markersize = 10, color = 'y', label='$d=2$')
# plt.loglog(size, time_3, marker='o', markersize = 7, color = 'g', label='$d=3$')
# plt.loglog(size, time_4, marker='x', markersize = 10, color = 'b', label='$d=4$')
# plt.xlabel('No. of variables, $n$', fontsize = 18)
# plt.ylabel('CPU time (sec)', fontsize = 18)
# plt.legend(loc = 2, fontsize = 15)
# plt.grid(True)
# plt.gca().set_xlim([50,20000])
# plt.xticks(fontsize = 15)
# plt.yticks(fontsize = 15)
#
# plt.subplot(122)
# plt.loglog(size, r_total_eval_1, marker='s', markersize = 7, color = 'r', label='$d=1$')
# plt.loglog(size, r_total_eval_2, marker='*', markersize = 10, color = 'y', label='$d=2$')
# plt.loglog(size, r_total_eval_3, marker='o', markersize = 7, color = 'g', label='$d=3$')
# plt.loglog(size, r_total_eval_3, marker='x', markersize = 10, color = 'b', label='$d=4$')
# plt.xlabel('No. of variables, $n$', fontsize = 18)
# plt.ylabel('Total no. of residual equations evaluated', fontsize = 18)
# plt.grid(True)
# plt.gca().set_xlim([50,20000])
# plt.legend(loc = 2, fontsize = 15)
# plt.xticks(fontsize = 15)
# plt.yticks(fontsize = 15)
# plt.show()

auto_group_time = np.array([62.8, 6.0, 6.7, 9.9, 2.6, 5.0, 83.0])
single_group_time = np.array([326.9, 631.6, 252.4, 497.0, 172.4, 170.9, 247.1])
cust_y_ax = [np.median(auto_group_time), np.median(single_group_time)]
data_to_plot1 = [single_group_time, auto_group_time]
plt.boxplot(data_to_plot1)
plt.xlabel('Approach no.', fontsize = 18)
plt.ylabel('Time (sec)', fontsize = 18)
#plt.ylabel('Wall time (sec)', fontsize = 18)
plt.xticks(fontsize = 15)
plt.yticks(cust_y_ax,fontsize = 15)
plt.show()
